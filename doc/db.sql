CREATE TABLE `progettotweb1`.`fornitori` (
	`pi` char(11) NOT NULL PRIMARY KEY,
	`nome` varchar(256) NOT NULL,
	`email` varchar(256) NOT NULL,
	`password` varchar(256) NOT NULL,
	`indirizzo` varchar(256) NOT NULL,
	`tel` int(10) NOT NULL,
    `costoConsegna` decimal(5,2) NOT NULL,
    `imgF` varchar(255)
) ENGINE=InnoDB;

CREATE TABLE `progettotweb1`.`admin` (
    `username` varchar(30) NOT NULL,
    `password` varchar(256) NOT NULL
) ENGINE=InnoDB;

CREATE TABLE `progettotweb1`.`utenti` (
	`id` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
	`username` varchar(30) NOT NULL,
	`email` varchar(50) NOT NULL,
	`password` varchar(256) NOT NULL,
	`nome` varchar(255) NOT NULL,
	`cognome` varchar(255) NOT NULL,
	`tel` int(10) NOT NULL
) ENGINE=InnoDB;

CREATE TABLE `progettotweb1`.`cibo` (
    `ciboID` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `nome` varchar(255) NOT NULL,
    `cat` varchar(255) NOT NULL,
    `prezzo` decimal(5,2) NOT NULL,
    `imgC` varchar(255)
) ENGINE=InnoDB;

CREATE TABLE `progettotweb1`.`offerte` (
    `ciboID` int(50) NOT NULL,
    `pi` char(11) NOT NULL
) ENGINE=InnoDB;

CREATE TABLE `progettotweb1`.`consegne` (
    `consegnaID` int  NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `pi` char(11) NOT NULL,
    `userID` int(50) NOT NULL,
    `data` date NOT NULL,
    `orario` time NOT NULL,
    `totale` decimal(5,2),
    `luogo` varchar(255) NOT NULL,
    `consegnato` int(1) NOT NULL
) ENGINE=InnoDB;

CREATE TABLE `progettotweb1`.`cosa` (
    `ciboID` int(50) NOT NULL,
    `consegnaID` int(50) NOT NULL
) ENGINE=InnoDB;

CREATE TABLE `progettotweb1`.`notificheUtente` (
    `notificaID` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `userID` int(50) NOT NULL,
    `valore` varchar(255) NOT NULL,
    `consegnaID` int NOT NULL
) ENGINE=InnoDB;

CREATE TABLE `progettotweb1`.`notificheFornitore` (
    `notificheID` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `pi` char(11) NOT NULL,
    `valore` varchar(255) NOT NULL,
    `consegnaID` int NOT NULL
) ENGINE=InnoDB;


