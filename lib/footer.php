<footer class="page-footer  light-green">
          <div class="container">
            <div class="row">
              <div class="col l4 s12">
                <h5 class="grey-text text-darken-3 center">Chi siamo</h5>
                <ul class="center">
                  <li class="grey-text text-darken-3"><a class="grey-text text-darken-3" href="../login/loginAdmin.php">Giulia Brugnatti</a></li>
                  <li class="grey-text text-darken-3">Filippo Paganelli</li>
                  <li class="grey-text text-darken-3">Linda Vitali</li>
                </ul>
              </div>
              <div class="col l4 s12">
                <h5 class="grey-text text-darken-3 center">Seguici su</h5>
                <ul class="center">
                  <lh><a class="grey-text text-lighten-3 " href="../HomeP/HomeP.php"><img class="z-depth-2" src="../../usedImages/facebook.png" alt="Facebook Link"></a></li>
                  <lh><a class="grey-text text-lighten-3 " href="https://www.instagram.com/uniichow/?utm_source=ig_profile_share&igshid=sybau2t1gbn"><img class="z-depth-2" src="../../usedImages/instagram.png" alt="Instagram Link"></a></li>
                  <lh><a class="grey-text text-lighten-3 " href="#!"><img class="z-depth-2" src="../../usedImages/linkedin.png" alt="Linkedin Link"></a></li>
                </ul>
                  <h5 class="grey-text text-darken-3 center">Vuoi essere dei nostri ?</h5>
                    <div class="center">
                  <a id="registraRist" class=" waves-effect deep-orange darken-4 btn" href="../registrazione/sign_fornitore.php">Registra il tuo ristorante</a> </br></br>
                  <a id="accediRist" class=" waves-effect deep-orange darken-4 btn" href="../login/loginFornitore.php">Accedi come ristoratore</a>
                  </div>
                  <br/>
                    </div>
              <div class="col l4 s12">
                <h5 class="grey-text text-darken-3 center">Note legali</h5>
                <ul class="center">
                  <li><a class="grey-text text-darken-3" href="#!">Informativa sulla privacy</a></li>
                  <li><a class="grey-text text-darken-3" href="#!">Termini e condizioni</a></li>
                  <li><a class="grey-text text-darken-3" href="#!">Cookie policy</a></li>
                </ul>
              </div>
            </div>
          </div>
          <div class="footer-copyright">
            <div class="container ">
          <a class="grey-text text-darken-3">UniChow Italy S.r.l. - P.IVA 07330799960</a>
            <a class="grey-text text-darken-3 right" href="#!">© 2019 Copyright Unichow</a>
            </div>
            </div>
</footer>


<!--JavaScript at end of body for optimized loading-->
<script type="text/javascript" src="../../lib/materialize/js/materialize.min.js"></script>
