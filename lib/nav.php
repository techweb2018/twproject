<style media="screen">
@font-face {
  font-family: titleFont;
  src: url(BETTALIA.ttf);
}
.brand-logo{
   font-family: titleFont;
   color: #FFFFFF;
   font-size: 50px !important;
   padding-left: 20px !important;
}
.badge{
  color: white !important;
  left: -100px;
}



</style>
<script type="text/javascript">
$(document).ready(function(){
  $('.sidenav').sidenav();
});
</script>

<div class="navbar">
  <nav>
    <div class="nav-wrapper deep-orange darken-2">
      <a href="../../src/HomeP/HomeP.php" class="brand-logo">UniChow</a>
      <a href="#" data-target="slide-out" class="sidenav-trigger"><i class="material-icons">menu</i></a>
      <ul class="right hide-on-med-and-down">
        <li id="noti"></li>
        <li> <a id="shopcart" href="#!"><i class="material-icons left">shopping_cartt</i>Carrello 0<span class="badge"></span></a><li>
        <li id="carr"></li>
        <li> <a id="logout" href="../../src/HomeP/HomeP.php?logout=1">LOGOUT</span></a><li>

      </ul>
    </div>
  </nav>

  <ul id="slide-out" class="sidenav">
    <li><a id="home" href="../../src/Fornitore/fornitore.php"><i class="material-icons left">home</i>Il mio profilo</a></li>
    <li id="notiside"><a id="openNotify" href="../../src/Fornitore/notiSide.php"><i class="material-icons left">mail</i>Notifiche<span id="badge" class="badge"></span></a></li>
    <li><a id="shopcart2" href="#!"><i class="material-icons left">shopping_cartt</i>Carrello</a></li>
    <li> <a id="logout" href="../../src/HomeP/HomeP.php?logout=1">LOGOUT</a><li>
      <li> <img src="../../images/food.jpg" alt="no"> </li>

  </ul>
  </div>
