#OBIETTIVO DELL'ELABORATO:
Scrivere una applicazione web accessibile e responsive che consenta di ordinare il pranzo da più fornitori (reali o
immaginari) e farselo recapitare in un posto specifico del nuovo campus: uno dei due ingressi oppure zone specifiche interne del campus.

##Deve esserci:
* Un amministratore del sistema (ovvero noi)
* Utenti fornitori che si possono registrare e fornire ciascuno un listino (modificabile da loro).
* Utenti Clienti con un carrello per gli acquisti (da un fornitore alla volta).
* Sistema di notifica

Le notifiche sono da implementare come aggiornamenti della pagina web e associabili all'invio di email o altro all'utente/fornitore.

La fase di pagamento può essere simulata senza integrare un sistema di pagamento.

##ELEMENTI WOW
* Sicurezza login PHP (salare le password) anche peparle
* AJAX per visualizzare notifiche senza ricaricare la pagina
* altre funzionalità aggiuntive come gestione dei fattorini, più punti di consegna nel campus, acquisti da più fornitori

#ARCHITETTURA:
Per realizzare l’elaborato si possono usare:

## Lato server 
* PHP
* framework php
## Lato client:
* Javascript
* Jquery
* Bootstrap (consigliato)
* CSS

Non usare altri stack. Eventuali altre tecnologie che vi vengono in mente vanno concordate coi docenti PRIMA di cominciare l’elaborato.

#PROGETTAZIONE:
* Mobile first
* User centered
* Accessibile

#APPROCCIO ADOTTATO (esempio 2)
* Creo personas e scenarios (almeno 3).
* Produco un design centrato sull’utente del punto 1 e genero i mockup.
* Faccio un focus group di controllo che valuta i mockup e modifico il design in base all’output del focus group.

#CONSEGNA
Sviluppare una relazione SU UNICA PAGINA, esclusi i mockup sia mobile che desktop (vanno consegnati a parte), dove si descrive il processo di sviluppo e progettazione.
I cambiamenti apportati in fase di design vanno indicati nella relazione (quelli fatti in seguito al focus group dopo le prima bozze di mockup).

Creare un repository contenente tutto il codice da condividere con i prof.

E' necessario ricevere valutazione positiva per sostenere lo scritto. La discussione del progetto si tiene qualche giorno prima della data dello scritto con tutti i componeneti del gruppo: è necessario iscriversi sul sito e-learning di tec web nel modulo e uno dei 3 del gruppo deve essere iscritto anche nell appello su almaesami.
