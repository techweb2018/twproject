<?php
    require('../../lib/db_connect.php');
    session_start();
    if (isset($_POST["name"]) && isset($_POST["prezzo"]) && isset($_POST["type"]) && isset($_POST["imgC"])){
       $stmt = $conn->prepare("INSERT INTO cibo (nome, cat, prezzo, imgC) VALUES (?, ?, ?, ?)");
       $stmt->bind_param("ssds", $nome, $tipo, $prezzo, $imgC);

       $nome = mysqli_real_escape_string($conn, $_POST["name"]);
       $tipo = mysqli_real_escape_string($conn, $_POST["type"]);
       $prezzo = mysqli_real_escape_string($conn, $_POST["prezzo"]);
       $imgC = $_POST["imgC"];

       $stmt->execute();
       $stmt->close();

       $query="SELECT MAX(ciboID) FROM cibo";
       $res = $conn->query($query);
       if($res !== FALSE){
         if($res->num_rows > 0){
           $row = $res->fetch_assoc();
           $stmt1 = $conn->prepare("INSERT INTO offerte (ciboID, pi) VALUES (?, ?)");
           $stmt1->bind_param("is", $row["MAX(ciboID)"], $_SESSION["pi"]);
           $stmt1->execute();
         }
       }
    }
?>

<!DOCTYPE html>
  <html lang="it-IT">
    <head>
      <meta charset="UTF-8"/>
      <meta name="author" content="Filippo Paganelli"/>
      <title>UniChow - MyProfile</title>
      <?php require('../../lib/header.php'); ?>
      <script src="../../lib/jquery-3.2.1.min.js"></script>
      <script>
        $(document).ready(function(){
          $('select').formSelect();
        });
      </script>
    </head>
    <body>
      <div class="section  deep-orange darken-2">
        <div class="row container">
          <header>
            <h1 class ="center-align">Unichow</h1>
          </header>
          </div>
      </div>
      <div id ="container">
        <div class="row container">
          <div class="row">
              <form class="col s12" action="#" method="post">
                <div class="row">
                  <div class="input-field col s3 offset-s3">
                    <select id="type" name="type">
                      <option value="" disabled selected>Choose your option</option>
                        <option value="Bevanda">Bevanda</option>
                        <option value="Piadina">Piadina</option>
                        <option value="Panino">Panino</option>
                        <option value="Primo">Primo</option>
                        <option value="Secondo">Secondo</option>
                        <option value="Dolce">Dolce</option>
                        <option value="Orientale">Orientale</option>
                      </select>
                      <label>Tipologia elemento</label>
                  </div>
                  <div class="input-field col s3">
                    <input id="name" name="name" type="text" class="validate">
                    <label for="name">Nome con Ingredienti</label>
                  </div>
                </div>
                <div class="row">
                  <div class="input-field col s3 offset-s3">
                    <input id="prezzo" name="prezzo" type="number" class="validate" min="0" step="0.01">
                    <label for="prezzo">Prezzo</label>
                    <span class="helper-text" data-error="wrong" data-success="right"> </span>
                  </div>
                  <div class="file-field input-field col s3">
                    <div class="btn">
                      <span>Immagine</span>
                      <input type="file">
                    </div>
                    <div class="file-path-wrapper">
                      <input class="file-path validate" type="text" id="imgC" name="imgC" >
                    </div>
                    <span class="helper-text" data-error="wrong" data-success="right"> </span>
                  </div>
                </div>
                <div class="row center-align">
                  <button class="btn waves-effect waves-light" type="submit" name="action" >Submit
                    <i class="material-icons right">create</i>
                  </button>
                </div>
              </form>
              <a href="../Fornitore/fornitore.php" class="secondary-content"> Torna alla tua pagina<i class="material-icons">fast_forward</i></a>
            </div>
        </div>
      </div>
      <?php
        require('../../lib/footer.php');
      ?>
    </body>
</html>
