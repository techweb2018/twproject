<?php
  require('../../lib/db_connect.php');
  session_start();
  if (isset($_POST["nome"]) && isset($_POST["cognome"]) && isset($_POST["email"]) && isset($_POST["pw"]) && isset($_POST["username"]) && isset($_POST["tel"])) {

   $stmt = $conn->prepare("INSERT INTO utenti (username, email, password, nome, cognome, tel) VALUES (?, ?, ?, ?, ?, ?)");
   $stmt->bind_param("sssssi", $username, $email, $pw, $nome, $cognome, $tel);

   // eliminazione caratteri pericolosi e hash password
   $username = mysqli_real_escape_string($conn, $_POST["username"]);
   $email = mysqli_real_escape_string($conn, $_POST["email"]);
   $pw = password_hash($_POST["pw"], PASSWORD_DEFAULT);
   $nome = mysqli_real_escape_string($conn, $_POST["nome"]);
   $cognome = mysqli_real_escape_string($conn, $_POST["cognome"]);
   $tel = mysqli_real_escape_string($conn, $_POST["tel"]);

   if($stmt->execute() === TRUE) {
     $query="SELECT MAX(id) FROM utenti";
     $res = $conn->query($query);
     if($res !== FALSE){
       if($res->num_rows > 0){
         $row = $res->fetch_assoc();
         $_SESSION["id"] = $row["MAX(id)"];
         $_SESSION["nome"] = $nome;
         $_SESSION["logged_in"] = TRUE;
         header("Location: ../Utonti/clienti.php");
       }
     }
   } else {
     $_SESSION["logged_in"] = FALSE;
   }
 }
?>

<!DOCTYPE html>
  <html lang="it-IT">
    <head>
      <meta charset="UTF-8"/>
      <meta name="description"
          content="Pagina di registrazione per i fornitori di cibo nella zona di Cesena"/>
      <meta name="author" content="Filippo Paganelli"/>
      <title>UniChow - MyProfile</title>
      <?php require('../../lib/header.php'); ?>
      <script src="../../lib/jquery-3.2.1.min.js"></script>
    </head>
    <body>
      <div class="section  deep-orange darken-2">
        <div class="row container">
          <header>
            <h1 class ="center-align">Unichow</h1>
          </header>
          </div>
      </div>
      <div id ="container">
        <div class="row container">
          <div class="row">
              <form class="col s12" method="post" action="#">
                <div class="row">
                  <div class="input-field col s3 offset-s3">
                    <input id="username" name="username" type="text" class="validate" required pattern=".{4,}">
                    <label for="username">Username</label>
                    <span class="helper-text" data-error="wrong" data-success="right"> </span>
                  </div>
                  <div class="input-field col s3">
                    <input id="password" name="pw" type="password" class="validate" required pattern=".{8,}">
                    <label for="password">Password</label>
                    <span class="helper-text" data-error="wrong" data-success="right"> </span>
                  </div>
                </div>
                <div class="row">
                  <div class="input-field col s3 offset-s3">
                    <input id="nome" name="nome" type="text" class="validate" required pattern=".{2,}">
                    <label for="nome">Nome</label>
                    <span class="helper-text" data-error="wrong" data-success="right"> </span>
                  </div>
                  <div class="input-field col s3">
                    <input id="cognome" name="cognome" type="text" class="validate" required pattern=".{2,}">
                    <label for="cognome">Cognome</label>
                    <span class="helper-text" data-error="wrong" data-success="right"> </span>
                  </div>
                </div>
                <div class="row">
                  <div class="input-field col s3 offset-s3">
                    <input id="email" name="email" type="email" class="validate" required pattern=".{4,}">
                    <label for="email">Email</label>
                    <span class="helper-text" data-error="wrong" data-success="right"> </span>
                  </div>
                  <div class="input-field col s3">
                    <input id="tel" name="tel" type="tel" class="validate"required pattern=".{8,}">
                    <label for="tel">Telefono</label>
                    <span class="helper-text" data-error="wrong" data-success="right"> </span>
                  </div>
                </div>
                <div class="row center-align">
                  <button class="btn waves-effect waves-light" type="submit" name="action" >Registrati
                    <i class="material-icons right">send</i>
                  </button>
                </div>
              </form>
            </div>
        </div>
      </div>
      <?php
        require('../../lib/footer.php');
      ?>
    </body>
</html>
