<?php
  require('../../lib/db_connect.php');
  session_start();
  if (isset($_POST["imgF"]) && isset($_POST["pi"]) && isset($_POST["nome_att"]) && isset($_POST["email"]) && isset($_POST["pw"]) && isset($_POST["ind"]) && isset($_POST["tel"]) && isset($_POST["costoConsegna"])) {

   $stmt = $conn->prepare("INSERT INTO fornitori (pi, nome, email, password, indirizzo, tel, costoConsegna, imgF) VALUES (?, ?, ?, ?, ?, ?, ?, ?)");
   $stmt->bind_param("issssids", $pi, $nome, $email, $pw, $ind, $tel, $costoConsegna, $imgF);

   // eliminazione caratteri pericolosi e hash password
   $pi = mysqli_real_escape_string($conn, $_POST["pi"]);
   $nome = mysqli_real_escape_string($conn, $_POST["nome_att"]);
   $email = mysqli_real_escape_string($conn, $_POST["email"]);
   $pw = password_hash($_POST["pw"], PASSWORD_DEFAULT);
   $ind = mysqli_real_escape_string($conn, $_POST["ind"]);
   $tel = mysqli_real_escape_string($conn, $_POST["tel"]);
   $costoConsegna = mysqli_real_escape_string($conn, $_POST["costoConsegna"]);
   $imgF = $_POST["imgF"];

   if($stmt->execute() === TRUE) {
     $_SESSION["pi"] = $pi;
     $_SESSION["logged_in"] = TRUE;
     header("Location: ../Fornitore/fornitore.php");
   } else {
     $_SESSION["logged_in"] = FALSE;
   }
 }
?>

<!DOCTYPE html>
  <html lang="it-IT">
    <head>
      <meta charset="UTF-8"/>
      <meta name="description"
          content="Pagina di registrazione per i fornitori di cibo nella zona di Cesena"/>
      <meta name="author" content="Filippo Paganelli"/>
      <title>UniChow - MyProfile</title>
      <?php require('../../lib/header.php'); ?>
      <script src="../../lib/jquery-3.2.1.min.js"></script>
    </head>
    <body>
      <div class="section  deep-orange darken-2">
        <div class="row container">
          <header>
            <h1 class ="center-align">Unichow</h1>
          </header>
          </div>
      </div>
      <div id ="container">
        <div class="row container">
          <div class="row">
              <form class="col s12" method="post" action="#">
                <div class="row">
                  <div class="input-field col s3 offset-s3">
                    <input id="pi" name="pi" type="number" class="validate" required pattern=".{11,}" min="11111111111" max="99999999999">
                    <label for="pi">Partita IVA</label>
                    <span class="helper-text" data-error="wrong" data-success="right"> </span>
                  </div>
                  <div class="input-field col s3">
                    <input id="first_name" name="nome_att" type="text" class="validate" required pattern=".{4,}">
                    <label for="first_name">Nome Attività</label>
                    <span class="helper-text" data-error="wrong" data-success="right"> </span>
                  </div>
                </div>
                <div class="row">
                  <div class="input-field col s3 offset-s3">
                    <input id="email" name="email" type="email" class="validate" required pattern=".{4,}">
                    <label for="email">Email</label>
                    <span class="helper-text" data-error="wrong" data-success="right"> </span>
                  </div>
                  <div class="input-field col s3">
                    <input id="password" name="pw" type="password" class="validate" required pattern=".{8,}">
                    <label for="password">Password</label>
                    <span class="helper-text" data-error="wrong" data-success="right"> </span>
                  </div>
                </div>
                <div class="row">
                  <div class="input-field col s3 offset-s3">
                    <input id="indirizzo" name="ind" type="text" class="validate" required pattern=".{4,}">
                    <label for="indirizzo">Indirizzo</label>
                    <span class="helper-text" data-error="wrong" data-success="right"> </span>
                  </div>
                  <div class="input-field col s3">
                    <input id="tel" name="tel" type="tel" class="validate"required pattern=".{8,}">
                    <label for="tel">Telefono</label>
                    <span class="helper-text" data-error="wrong" data-success="right"> </span>
                  </div>
                </div>
                <div class="row">
                  <div class="file-field input-field col s3 offset-s3">
                    <div class="btn">
                      <span>Immagine</span>
                      <input type="file">
                    </div>
                    <div class="file-path-wrapper">
                      <input class="file-path validate" type="text" id="imgF" name="imgF" >
                    </div>
                    <span class="helper-text" data-error="wrong" data-success="right"> </span>
                  </div>
                  <div class="input-field col s3">
                    <input id="costoConsegna" name="costoConsegna" type="number" class="validate" required pattern=".{1,}" min="0" step="0.01" >
                    <label for="costoConsegna">Costo Consegna</label>
                    <span class="helper-text" data-error="wrong" data-success="right"> </span>
                  </div>
                </div>
                <div class="row center-align">
                  <button class="btn waves-effect waves-light" type="submit" name="action" >Registrati
                    <i class="material-icons right">send</i>
                  </button>
                </div>
              </form>
            </div>
        </div>
      </div>
      <?php
        require('../../lib/footer.php');
      ?>
    </body>
</html>
