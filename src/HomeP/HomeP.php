<?php
  session_start();
  if(!isset($_SESSION["logged_inF"])){
      $_SESSION["logged_inF"] = FALSE;
  }

  if(!isset($_SESSION["logged_inC"])){
      $_SESSION["logged_inC"] = FALSE;
  }

  $user_access = "first_time_access";
  $user_value = "no";
  setcookie($user_access, $user_value, time()+10);
  if(isset($_GET["logout"])){
    if($_GET["logout"] == 1){
      session_unset();
      session_destroy();

      $_GET["logout"] = 0;
      session_start();
      if(!isset($_SESSION["logged_inF"])){
          $_SESSION["logged_inF"] = FALSE;
      }

      if(!isset($_SESSION["logged_inC"])){
          $_SESSION["logged_inC"] = FALSE;
      }
    }
  }

 ?>


<!DOCTYPE html>
  <html lang="it-IT">
    <head>
      <?php require('../../lib/header.php'); ?>
      <script src="../../lib/jquery-3.2.1.min.js"></script>
      <script src="home.js"></script>
    </head>
<style media="screen">
@media screen and (max-width: 600px) {
.foodAnimation{
 visibility: hidden;
  }
}
</style>
    <body>
    <div class="section deep-orange darken-2">
      <div class="row container">
        <header>
        <h1 class ="center-align">Unichow</h1>
          </header>
          <p class="center-align">
          <a class="waves-effect deep-orange darken-4 btn" href="../login/loginUtente.php">LOGIN</a>
          <a class=" waves-effect deep-orange darken-4 btn" href="../registrazione/sign_clienti.php">REGISTRATI</a>
          </p>
      </div>
    </div>

    <div class="parallax-container">
    <div class="parallax"><img src="../../usedImages/burgerLOGO.png"></div>
    </div>

    <div id ="container" class="section yellow lighten-2">
      <div class="row container">
      <section>
        <p id="slogan">Ordina il tuo cibo preferito e ricevilo direttamente nel campus</p>
        <div class="foodAnimation" id ="burgerAnimation"><img src="../../usedImages/burger.png" width=30 height=30> </div>
        <div class="foodAnimation" id ="drinkAnimation"><img src="../../usedImages/drink.png" width=30 height=30> </div>
        <div class="foodAnimation" id ="friesAnimation" align="right"><img src="../../usedImages/fries.png" width=30 height=30> </div>

        <div class="foodAnimation" id ="burgerAnimation1" align="bottom|left"><img src="../../usedImages/burger.png" width=30 height=30> </div>
        <div class="foodAnimation" id ="drinkAnimation1" align="top|right"><img src="../../usedImages/drink.png" width=30 height=30> </div>
        <div class="foodAnimation" id ="friesAnimation1" align="middle|left"><img src="../../usedImages/fries.png" width=30 height=30> </div>


        <div class="foodAnimation" id ="burgerAnimation2" ><img src="../../usedImages/burger.png" width=30 height=30> </div>
        <div class="foodAnimation" id ="drinkAnimation2" align="middle|top"><img src="../../usedImages/drink.png" width=30 height=30> </div>
        <div class="foodAnimation" id ="friesAnimation2" align="middle"><img src="../../usedImages/fries.png" width=30 height=30> </div>

      </section>
  </div>
</div>

<?php
  require('../../lib/footer.php');
    if(!isset($_COOKIE[$user_access])){
    ?>
    <script type="text/javascript">
      $(document).ready(function(){
        alert("Unichow tratta le informazioni relative al tuo accesso attraverso l'utilizzo di cookies. Proseguendo la navigazione acconsenti pertanto l'utilizzo di tali cookies.");
      });
    </script>
    <?php
  }
?>
</body>
</html>
