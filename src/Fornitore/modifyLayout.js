function updateNotify(size){
    $.getJSON("getNumTot.php", function(data){
      $("#badge").text(data[0]["COUNT(notificheID)"]);
    });
}

function showNotify(){
  $("#notifySpace").show();
  $.getJSON("getNotify.php", function(data){
    var html_code = '<li><table class="responsive-table bordered centered white"><thead>'+
    '<tr><th>Consegna</th>'+
        '<th>Data</th>'+
        '<th>Ora</th>'+
        '<th>Descrizione</th>'+
        '<th>Cancella</th></tr></thead></li><li><tbody>';

    updateNotify();
    for(var i = 0; i < data.length; i++){
      html_code += '<tr><td>'+data[i]["consegnaID"]+'</td><td>'+data[i]["data"]+'</td><td>'+data[i]["ora"]+'</td><td>'+data[i]["valore"]+'</td><td>';
      html_code += '<a href="#" onclick="deleteNotify('+data[i]["notificheID"]+')" class="secondary-content"><i class="material-icons">delete</i></a></td></tr>';
    }
    html_code += '</tbody></table>';
    $("#notifySpace").html(html_code);
  });
}

function deleteNotify(notificheID){
  obj = {"notificheID" : notificheID};
  dbParam = JSON.stringify(obj);
  var xmlhttp = new XMLHttpRequest();
  xmlhttp.onreadystatechange = function() {
    if(this.readyState == 4 && this.status == 200) {
      $("#notifySpace").empty();
      showNotify();
      if($("#badge").text() == "1"){
            $("#badge").text("0");
      }
    }
  };
  xmlhttp.open("GET", "deleteNotify.php?id="+dbParam, true);
  xmlhttp.send();
}


$(document).ready(function(){

    var notify = false;
    $("#shopcart").hide();

    var html_code = '<a id="openNotify" href="#"><i class="material-icons left">mail</i>Notifiche<span id="badge" class="badge">0</span></a>';
    $("#noti").html(html_code);
    updateNotify();


    $("#openNotify").click(function(){
      if(notify == false){
        notify = true;
        showNotify();
      } else {
        notify = false;
        $("#notifySpace").hide();
        updateNotify();
      }
    });

});
