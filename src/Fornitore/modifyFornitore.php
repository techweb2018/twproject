<?php
  require('../../lib/db_connect.php');
  session_start();
  if (isset($_POST["imgF"]) && isset($_POST["nome_att"]) && isset($_POST["email"]) && isset($_POST["ind"]) && isset($_POST["tel"]) && isset($_POST["costoConsegna"])) {


   $stmt = $conn->prepare("UPDATE fornitori SET nome = ?, email = ?, indirizzo = ?, tel = ?, costoConsegna = ?, imgF = ? WHERE pi = ?");
   $stmt->bind_param("sssidsi", $nome, $email, $ind, $tel, $costoConsegna, $imgF, $pi);

   // eliminazione caratteri pericolosi e hash password
   $pi = $_SESSION["pi"];
   $imgF = $_POST["imgF"];
   $nome = mysqli_real_escape_string($conn, $_POST["nome_att"]);
   $email = mysqli_real_escape_string($conn, $_POST["email"]);
   $ind = mysqli_real_escape_string($conn, $_POST["ind"]);
   $tel = mysqli_real_escape_string($conn, $_POST["tel"]);
   $costoConsegna = mysqli_real_escape_string($conn, $_POST["costoConsegna"]);

   if($stmt->execute() === TRUE) {
     header("Location: ../Fornitore/fornitore.php");
   } else {
     $_SESSION["logged_in"] = FALSE;
   }
 }
?>

<!DOCTYPE html>
  <html lang="it-IT">
    <head>
      <meta charset="UTF-8"/>
      <meta name="description"
          content="Pagina di registrazione per i fornitori di cibo nella zona di Cesena"/>
      <meta name="author" content="Filippo Paganelli"/>
      <title>UniChow - MyProfile</title>
      <?php require('../../lib/header.php'); ?>
      <script src="../../lib/jquery-3.2.1.min.js"></script>
    </head>
    <body>
      <div class="section  deep-orange darken-2">
        <div class="row container">
          <header>
            <h1 class ="center-align">Unichow</h1>
          </header>
          </div>
      </div>
      <div id ="container">
        <div class="row container">
          <div class="row">
              <form class="col s12" method="post" action="#">
                <div class="row">
                  <div class="input-field col s3 offset-s3">
                    <input id="first_name" name="nome_att" type="text" value="<?php echo $_SESSION["nome"]; ?>" class="validate" required pattern=".{4,}">
                    <label for="first_name">Nome Attività</label>
                    <span class="helper-text" data-error="wrong" data-success="right"> </span>
                  </div>
                  <div class="input-field col s3">
                    <input id="email" name="email" type="email" value="<?php echo $_SESSION["email"]; ?>" class="validate" required pattern=".{4,}">
                    <label for="email">Email</label>
                    <span class="helper-text" data-error="wrong" data-success="right"> </span>
                  </div>
                </div>
                <div class="row">
                  <div class="input-field col s3 offset-s3">
                    <input id="indirizzo" name="ind" type="text" value="<?php echo $_SESSION["indirizzo"]; ?>" class="validate" required pattern=".{4,}">
                    <label for="indirizzo">Indirizzo</label>
                    <span class="helper-text" data-error="wrong" data-success="right"> </span>
                  </div>
                  <div class="input-field col s3">
                    <input id="tel" name="tel" type="tel" class="validate" value="<?php echo $_SESSION["tel"]; ?>" required pattern=".{8,}">
                    <label for="tel">Telefono</label>
                    <span class="helper-text" data-error="wrong" data-success="right"> </span>
                  </div>
                </div>
                <div class="row">
                  <div class="input-field col s3 offset-s3">
                    <input id="costoConsegna" name="costoConsegna" type="number" value="<?php echo $_SESSION["costoConsegna"]; ?>" class="validate" required pattern=".{1,}" min="0" step="0.01" >
                    <label for="costoConsegna">Costo Consegna</label>
                    <span class="helper-text" data-error="wrong" data-success="right"> </span>
                  </div>
                  <div class="file-field input-field col s3">
                    <div class="btn">
                      <span>Immagine</span>
                      <input type="file">
                    </div>
                    <div class="file-path-wrapper">
                      <input class="file-path validate" type="text" id="imgF" name="imgF" >
                    </div>
                    <span class="helper-text" data-error="wrong" data-success="right"> </span>
                  </div>
                </div>
                <div class="row center-align">
                  <button class="btn waves-effect waves-light" type="submit" name="action">Conferma
                    <i class="material-icons right">send</i>
                  </button>
                </div>
              </form>
            </div>
        </div>
      </div>
      <?php
        require('../../lib/footer.php');
      ?>
    </body>
</html>
