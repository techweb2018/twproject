function updateNotify(size){
    $.getJSON("getNumTot.php", function(data){
      $("#badge").text(data[0]["COUNT(notificheID)"]);
    });
}

function showNotify(){
  $("#notifySpace").show();
  $.getJSON("getNotify.php", function(data){

    var html_code = "";

    updateNotify();
    for(var i = 0; i < data.length; i++){
      html_code += '<li class="collection-item"><div>Consegna: '+data[i]["consegnaID"]+'</br>Data: '+data[i]["data"]+' Ora: '+data[i]["ora"]+'</br>'+data[i]["valore"];
      html_code += '<a href="#" onclick="deleteNotify('+data[i]["notificheID"]+')" class="secondary-content"><i class="material-icons">delete</i></a></div></li>';
    }

    $("#notifySpace").html(html_code);
  });
}

function deleteNotify(notificheID){
  obj = {"notificheID" : notificheID};
  dbParam = JSON.stringify(obj);
  var xmlhttp = new XMLHttpRequest();
  xmlhttp.onreadystatechange = function() {
    if(this.readyState == 4 && this.status == 200) {
      $("#notifySpace").empty();
      showNotify();
      if($("#badge").text() == "1"){
            $("#badge").text("0");
      }
    }
  };
  xmlhttp.open("GET", "deleteNotify.php?id="+dbParam, true);
  xmlhttp.send();
}


$(document).ready(function(){

    var notify = false;
    $("#shopcart").hide();

    var html_code = '<a id="openNotify" href="#"><i class="material-icons left">mail</i>Notifiche<span id="badge" class="badge">0</span></a>';
    $("#noti").html(html_code);
    updateNotify();


    $("#openNotify").click(function(){
      if(notify == false){
        notify = true;
        showNotify();
      } else {
        notify = false;
        $("#notifySpace").hide();
        updateNotify();
      }
    });

});
