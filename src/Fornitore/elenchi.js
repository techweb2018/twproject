function showOfferte(){
    var result;

    $.getJSON("elencoOfferte.php",function(data){
      var html_code = "";
      for(var i = 0; i < data.length; i++){
        html_code += '<li class="collection-item avatar"><img src="../../images/'+data[i]["imgC"]+'" alt="" class="circle" > Nome: '+data[i]["nome"]+' Categoria: '+data[i]["cat"]+' Prezzo: '+data[i]["prezzo"]+'<div>';
        html_code += '<a onclick="deleteOfferta('+data[i]["ciboID"]+')" href="#!" class="secondary-content"><i class="material-icons">delete</i></a></div></li>';
      }
      result = html_code;
      $("#offerte").html(html_code);
    });

    return result;
}

function showOrdini(){
    var result;

    $.getJSON("elencoOrdini.php", function(data){
      var html_code = "";
      console.log(data.length);
      for(var i = 0; i < data.length; i++){
        html_code += '<li class="collection-item"><div>Ordine: '+data[i]["consegnaID"]+' Luogo: '+data[i]["luogo"]+' Data: '+data[i]["data"]+' Ora: '+data[i]["orario"]+' Totale: '+data[i]["totale"];
        html_code += '<a href="consegna.php?id='+data[i]["consegnaID"]+'" class="secondary-content"><i class="material-icons">settings</i></a></div></li>';
      }
      result = html_code;
      $("#ordini").html(html_code);
    });

      return result;
  }

  function deleteOfferta(ciboid){
    obj = {"ciboid" : ciboid};
    dbParam = JSON.stringify(obj);
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
      if(this.readyState == 4 && this.status == 200) {
        showOfferte();
      }
    };
    xmlhttp.open("GET", "deleteOfferta.php?id="+dbParam, true);
    xmlhttp.send();
    console.log(ciboid);
  }

function deleteAccount(pi){
  obj = {"pi" : pi};
  dbParam = JSON.stringify(obj);
  var xmlhttp = new XMLHttpRequest();
  xmlhttp.onreadystatechange = function() {
    if(this.readyState == 4 && this.status == 200){
      console.log("Account eliminato");
      console.log(pi);
    }
  };
  xmlhttp.open("GET", "deleteAccount.php?pi="+dbParam, true);
  xmlhttp.send();
}

function hideAttempt(){
  $("#deleteAcc").hide();
}

function showAttempt(pi){
  $("#deleteAcc").show();
  html_code = '<div class="card blue-grey darken-1">'+
                '<div class="card-content white-text">'+
                '<span class="card-title">Eliminazione Account</span>'+
                '<p>Confermare se si vuole procedere con la eliminazione del proprio account.</p>'+
                '</div><div class="card-action"><a href="../HomeP/HomeP.php" onclick="deleteAccount('+pi+')" >Si</a><a href="#!" onclick=" hideAttempt()">No</a></div></div>';
  $("#deleteAcc").html(html_code);
}

$(document).ready(function(){
  showOfferte();
  showOrdini();

});
