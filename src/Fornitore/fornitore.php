<?php
  session_start();
  require('../../lib/db_connect.php');

  if($stmt = $conn->prepare("SELECT pi, nome, email, indirizzo, tel, costoConsegna, imgF FROM fornitori WHERE pi = ?")){
    $stmt->bind_param('s', $_SESSION["pi"]);
    $stmt->execute();
    $stmt->store_result();
    $stmt->bind_result($pi, $nome, $email, $ind, $tel, $costoConsegna, $imgF);
    $stmt->fetch();
    $_SESSION["pi"]=$pi;
    $_SESSION["nome"] = $nome;
    $_SESSION["email"]= $email;
    $_SESSION["indirizzo"]= $ind;
    $_SESSION["tel"]=$tel;
    $_SESSION["costoConsegna"]=$costoConsegna;
    $_SESSION["imgF"] = $imgF;
  }
?>

<!DOCTYPE html>
  <html lang="it-IT">
    <head>
      <meta charset="UTF-8"/>
      <meta name="description"
          content="Pagina Personale dell'utente registrato"/>
      <meta name="author" content="Filippo Paganelli"/>
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>UniChow</title>
      <?php require('../../lib/header.php'); ?>
      <script src="../../lib/jquery-3.2.1.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
      <script src="elenchi.js"></script>
      <script src="modifyLayout.js"></script>
      <style media="screen">
        body{
          background-image: url("../../images/<?php echo $imgF; ?>");
        }
      </style>
    </head>
    <body>
      <?php require('../../lib/nav.php'); ?>
      <div id="container" class="container">
        <div id="notifySpace" class="row">

        </div>
        <div class="row">
          <div class="col l6 m6">
            <ul class="collection with-header">
              <li class="collection-header"><h4>I miei dati:</h4></li>
              <li class="collection-item">Partita IVA: <?php echo $pi; ?></li>
              <li class="collection-item">Nome: <?php echo $nome; ?></li>
              <li class="collection-item">Indirizzo: <?php echo $ind; ?></li>
              <li class="collection-item">Email: <?php echo $email; ?></li>
              <li class="collection-item">Telefono: <?php echo $tel; ?></li>
              <li class="collection-item">Costo Consegna: <?php echo $costoConsegna; ?> €</li>
              <li class="collection-item"><div>Modifica Dati<a href="modifyFornitore.php" class="secondary-content"><i class="material-icons">settings</i></a></div></li>
              <li class="collection-item"><div>Modifica Password<a href="modifyPassword.php" class="secondary-content"><i class="material-icons">settings</i></a></div></li>
              <li class="collection-item"><div>Elimina Account<a href="#!" onclick="showAttempt(<?php echo $pi; ?>)"class="secondary-content"><i class="material-icons">delete</i></a></div></li>
              <ul id="deleteAcc"></ul>
            </ul>
          </div>

          <div class="col l6 m6">
            <ul class="collection with-header">
              <li class="collection-header"><h4>Le mie offerte:</h4></li>
                <ul id="offerte"></ul>
              <li class="collection-item"><div>Aggiungi una nuova offerta<a href="../registrazione/food_add.php" class="secondary-content"><i class="material-icons">edit</i></a></div></li>
            </ul>
          </div>
          </div>
          <div class="row">

          </div>
            <div class="row">
              <div class="col s12 m12">
                <ul class="collection with-header">
                  <li class="collection-header"><h4>I miei ordini:</h4></li>
                  <ul id="ordini"></ul>
                </ul>
              </div>
            </div>
          </div>
      <?php require('../../lib/footer.php');?>
      <script type="text/javascript">
      $("#asd").hide();
      $("#registraRist").hide();
      $("#accediRist").hide();
      </script>
    </body>
  </html>
