<?php
  session_start();
  require('../../lib/db_connect.php');
 ?>

 <!DOCTYPE html>
   <html lang="it-IT">
     <head>
       <meta charset="UTF-8"/>
       <meta name="description"
           content="Pagina Personale dell'utente registrato"/>
       <meta name="author" content="Filippo Paganelli"/>
       <meta name="viewport" content="width=device-width, initial-scale=1.0">
       <title>UniChow</title>
       <?php require('../../lib/header.php'); ?>
       <script src="../../lib/jquery-3.2.1.min.js"></script>
        <script src="modifyLayout.js"></script>
       <script type="text/javascript">
          function consegna(consegnaid){
            console.log(consegnaid);
            obj = {"consegnaid" : consegnaid};
            dbParam = JSON.stringify(obj);
            var xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function() {
              if(this.readyState == 4 && this.status == 200) {
                $("#title").append("CONSEGNATO");
              }
            };
            xmlhttp.open("GET", "effettuaConsegna.php?id="+dbParam, true);
            xmlhttp.send();
          }
       </script>
     </head>
     <body>
       <?php
          require('../../lib/nav.php');
          $q = $_REQUEST["id"];
          //selezione dei dati dell'utente che ha effettuato l'ordine
          $query = "SELECT * FROM consegne JOIN utenti ON consegne.userID = utenti.id WHERE consegnaID = $q";
          $res = $conn->query($query);
          if($res !== FALSE){
            $row = $res->fetch_assoc();
            $_SESSION["totale"]=$row["totale"];
            $_SESSION["cons"] = $q;
       ?>
       <div class="container">
         <div class="row">
             <div class="col s12">
               <ul class="collection with-header">
                 <li id="title" class="collection-header"><h4>Ordine: <?php echo $row["consegnaID"]; ?> </h4></li>
                 <li class="collection-item">Cliente: <?php echo $row["nome"]." ".$row["cognome"]; ?></li>
                 <li class="collection-item">Luogo: <?php echo $row["luogo"]; ?></li>
                 <li class="collection-item">Data: <?php echo $row["data"]; ?> Ora: <?php echo $row["orario"];?></li>
               </ul>
     <?php } //chiusura if?>
            </div>
        </div>
        <div class="row">
            <div class="col s12">
              <ul class="collection with-header">
                <li class="collection-header"><h4>Contenuto: </h4></li>
                <?php
                  //selezione dei componenti dell'ordine
                  $query="SELECT * FROM cibo JOIN cosa ON cibo.ciboID = cosa.ciboID WHERE cosa.consegnaID = $q";
                  $res=$conn->query($query);
                  if($res !== FALSE) {
                    while($row = $res->fetch_assoc()){
                ?>
                        <li class="collection-item">Nome: <?php echo $row["nome"]; ?> Prezzo: <?php echo $row["prezzo"]; ?> Quantità: <?php echo $row["quantita"]; ?></li>
                <?php
                    } //chiususra while
                  } //chiusura if
                ?>
                <li class="collection-item">Totale: <?php echo $_SESSION["totale"]; ?></li>
              </ul>
            </div>
          </div>
        <div class="row">
          <div class="col s3 offset-s4">
            <a href="#!" onclick="consegna(<?php echo $_SESSION["cons"]; ?>)" id="consegna" class=" btn secondary-content">Consegna l'ordine</a>
          </div>
        </div>
        <div class="row">
          <div class="col s3 offset-s4">
            <a href="fornitore.php"  id="consegna" class=" btn secondary-content">Ritorna alla tua pagina </a>
          </div>
        </div>
      </div>
      <?php require('../../lib/footer.php');?>
     </body>
  </html>
