<!DOCTYPE html>
  <html lang="it-IT">
    <head>
      <meta charset="UTF-8"/>
      <meta name="description"
          content="Pagina Personale dell'utente registrato"/>
      <meta name="author" content="Filippo Paganelli"/>
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>UniChow</title>
      <?php require('../../lib/header.php'); ?>
      <script src="../../lib/jquery-3.2.1.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
      <script src="modifyLayoutNav.js"></script>
      <style media="screen">
        body{
          background-image: url("../../images/<?php echo $imgF; ?>");
        }
      </style>
    </head>
    <body>
      <?php require('../../lib/nav.php'); ?>
      <div id="container" class="container">
        <div class="row">
          <div class="col s12 m12">
            <ul class="collection with-header">
              <li class="collection-header"><h4>Notifiche:</h4></li>
              <ul id="notifySpace"></ul>
            </ul>
          </div>
        </div>
      </div>

      <?php require('../../lib/footer.php');?>
      <script type="text/javascript">
      $("#asd").hide();
      $("#registraRist").hide();
      $("#accediRist").hide();

      showNotify();
      </script>
    </body>
  </html>
