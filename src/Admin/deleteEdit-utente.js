function usrDelBut(idUsr){
  if(confirm("Sei sicuro di volerlo eliminare ?")){
    obj = {"idUsr" : idUsr};
    dbParam = JSON.stringify(obj);
    console.log(dbParam);
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
      if(this.readyState == 4 && this.status == 200) {
        riaggiornareLista();
      }
    };
    xmlhttp.open("GET", "deleteEditApi-utente.php?id="+dbParam, true);
    xmlhttp.send();

  }
}

function riaggiornareLista (){
  var page = 0;
  var num_pages = 3;

  $.getJSON("elencoUtentiApi.php?request=users&page="+page, function(data) {
  var html_code = "";
  console.log(data.length);
  for(var i = 0; i < data.length; i++){
    html_code += '<li class="collection-item avatar"> <i class="material-icons circle">person</i>';
    html_code += '<span class="title">Username: '+data[i]["username"]+'</span>'+'<p>Email: '+data[i]["email"]+'</p>'+'<p>Nome: '+data[i]["nome"]+'</p>'+'<p>Cognome: '+data[i]["cognome"]+'</p>'+'<p>Telefono: '+data[i]["tel"]+'</p>';
    html_code += '<a href="#!" class="secondary-content"><i  onclick="usrEditBut('+data[i]["id"]+')" class="material-icons">edit</i><i  onclick="usrDelBut('+data[i]["id"]+')" class="material-icons">delete</i></a><div id="'+data[i]["id"]+'"></div></li>';
  }
  $(".collection").html(html_code);
});

$.getJSON("elencoUtentiApi.php?request=num_pages_users", function(data) {
    var num_pages = data["num_pages_users"];

    html_code = '<li class="waves-effect"><a><i class="material-icons">chevron_left</i></a></li>';
    for(let i = 0; i < num_pages; i++){
      html_code += '<li class="waves-effect"><a>'+(i+1)+'</a></li>';
    }
    html_code += '<li class="waves-effect"><a><i class="material-icons">chevron_right</i></a></li>';

    $(".numb ul").html(html_code);

    manage_pages(page, num_pages);

    $(".pagination li").click(function(){
      //controllo se ha classe active o disabled
      if(!$(this).hasClass("disabled") && !$(this).hasClass("active")){
        var contenuto = $(this).find("a").text();
        switch(contenuto) {
          case "chevron_right":
            page+=1;
            break;
          case "chevron_left":
            page-=1;
            break;
          default:
            page = contenuto -1;
        }

        $.getJSON("elencoUtentiApi.php?request=users&page="+page, function(data) {
        var html_code = "";
        for(var i = 0; i < data.length; i++){
          html_code += '<li class="collection-item avatar"> <i class="material-icons circle">person</i>';
          html_code += '<span class="title">Username: '+data[i]["username"]+'</span>'+'<p>Email: '+data[i]["email"]+'</p>'+'<p>Nome: '+data[i]["nome"]+'</p>'+'<p>Cognome: '+data[i]["cognome"]+'</p>'+'<p>Telefono: '+data[i]["tel"]+'</p>';
          html_code += '<a href="#!" class="secondary-content"><i onclick="usrEditBut('+data[i]["id"]+')" class="material-icons">edit</i><i  onclick="usrDelBut('+data[i]["id"]+')" class="material-icons">delete</i></a><div id="'+data[i]["id"]+'"></div></li>';
        }
        $(".collection").html(html_code);
      });
        manage_pages(page, num_pages);
      }
    });

  });
}
