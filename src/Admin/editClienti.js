function usrEditBut(piForn){
  console.log("dentro");
  $.getJSON("elencoUtentiApi.php?request=user&ids="+piForn, function(data) {
    html_code = '<div class="row"><div class="col s12 m6"><div id="crd" class="card white"><div class="card-content orange-text"><span class="card-title">Modifica</span><p><input id="nome" name="nome" value="'+data[0]["nome"]+'" type="text" class="validate"><label for="text">Nome</label><input id="cognome" name="cognome" value="'+data[0]["cognome"]+'" type="text" class="validate"><label for="cognome">Cognome</label><input id="tel" value="'+data[0]["tel"]+'" name="tel" type="text" class="validate"><label for="tel">Telefono</label><input id="id" value="'+data[0]["id"]+'" name="id" type="text" hidden><label for="id">id</label><input id="username" name="username" type="text" value="'+data[0]["username"]+'" class="validate"><label for="username">Username</label><input id="email" name="email" value="'+data[0]["email"]+'" type="text" class="validate"><label for="text">Email</label></p></div><div class="card-action"><a onclick="salva()" href="#">Salva</a><a onclick="annulla()" href="#">Annulla</a></div></div></div></div>';
    var id = data[0]["id"];
    $("#"+id).html(html_code);
});
}

function annulla(){
  $("#crd").hide();
}

function salva(){
  var $items = $('#nome,#cognome,#email,#tel,#username,#id');
  var obj = {}
  $items.each(function() {
  obj[this.id] = $(this).val();
});

  var dbParam = JSON.stringify(obj);

  var xmlhttp = new XMLHttpRequest();
  xmlhttp.onreadystatechange = function() {
    if(this.readyState == 4 && this.status == 200) {
      riaggiornareLista();
    }
  };

  xmlhttp.open("GET", "editClientiApi.php?userIds="+dbParam, true);
  xmlhttp.send();

  $("#crd").hide();
}


function riaggiornareLista (){
  var page = 0;
  var num_pages = 3;

  $.getJSON("elencoUtentiApi.php?request=users&page="+page, function(data) {
  var html_code = "";
  console.log(data.length);
  for(var i = 0; i < data.length; i++){
    html_code += '<li class="collection-item avatar"> <i class="material-icons circle">person</i>';
    html_code += '<span class="title">Username: '+data[i]["username"]+'</span>'+'<p>Email: '+data[i]["email"]+'</p>'+'<p>Nome: '+data[i]["nome"]+'</p>'+'<p>Cognome: '+data[i]["cognome"]+'</p>'+'<p>Telefono: '+data[i]["tel"]+'</p>';
    html_code += '<a href="#!" class="secondary-content"><i  onclick="usrEditBut('+data[i]["id"]+')" class="material-icons">edit</i><i  onclick="usrDelBut('+data[i]["id"]+')" class="material-icons">delete</i></a><div id="'+data[i]["id"]+'"></div></li>';
  }
  $(".collection").html(html_code);
});

$.getJSON("elencoUtentiApi.php?request=num_pages_users", function(data) {
    var num_pages = data["num_pages_users"];

    html_code = '<li class="waves-effect"><a><i class="material-icons">chevron_left</i></a></li>';
    for(let i = 0; i < num_pages; i++){
      html_code += '<li class="waves-effect"><a>'+(i+1)+'</a></li>';
    }
    html_code += '<li class="waves-effect"><a><i class="material-icons">chevron_right</i></a></li>';

    $(".numb ul").html(html_code);

    manage_pages(page, num_pages);

    $(".pagination li").click(function(){
      //controllo se ha classe active o disabled
      if(!$(this).hasClass("disabled") && !$(this).hasClass("active")){
        var contenuto = $(this).find("a").text();
        switch(contenuto) {
          case "chevron_right":
            page+=1;
            break;
          case "chevron_left":
            page-=1;
            break;
          default:
            page = contenuto -1;
        }

        $.getJSON("elencoUtentiApi.php?request=users&page="+page, function(data) {
        var html_code = "";
        for(var i = 0; i < data.length; i++){
          html_code += '<li class="collection-item avatar"> <i class="material-icons circle">person</i>';
          html_code += '<span class="title">Username: '+data[i]["username"]+'</span>'+'<p>Email: '+data[i]["email"]+'</p>'+'<p>Nome: '+data[i]["nome"]+'</p>'+'<p>Cognome: '+data[i]["cognome"]+'</p>'+'<p>Telefono: '+data[i]["tel"]+'</p>';
          html_code += '<a href="#!" class="secondary-content"><i onclick="usrEditBut('+data[i]["id"]+')" class="material-icons">edit</i><i  onclick="usrDelBut('+data[i]["id"]+')" class="material-icons">delete</i></a><div id="'+data[i]["id"]+'"></div></li>';
        }
        $(".collection").html(html_code);
      });
        manage_pages(page, num_pages);
      }
    });

  });
}
