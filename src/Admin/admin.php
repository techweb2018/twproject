<!DOCTYPE html>
  <html lang="it-IT">
    <head>
      <?php require('../../lib/header.php'); ?>
      <script src="../../lib/jquery-3.2.1.min.js"></script>

      <style media="screen">
        body{
          background-image: url("../../usedImages/burgerLOGO.png");
        }
      </style>
    </head>

    <body>
      <?php require('../../lib/nav.php'); ?>

      <div class="container">
      <div class="row">
    <div class="col s12 l12 center-align">
      <ul class="collection with-header">
    <li class="collection-header"><h3>Bentornato!</h3></li>
    <li class=" collection-item"><div><h4>Visualizza fornitori<a href="elencoFornitori.php" class="secondary-content"><i class="material-icons">send</i></h4></a></div></li>
    <li class="collection-item"><div><h4>Visualizza utenti<a href="elencoUtenti.php" class="secondary-content"><i class="material-icons">send</i></h4></a></div></li>
  </ul>
</div>


</div>
</div>
    <?php require('../../lib/footer.php');?>
    <script type="text/javascript">
    $("#accedi").hide();
    $("#noti").hide();
    $("#shopcart").hide();
    $("#registraRist").hide();
    $("#accediRist").hide();
    $("#shopcart2").hide();
    </script>
  </body>
</html>
