<!DOCTYPE html>
  <html lang="it-IT">
    <head>

      <?php require('../../lib/header.php'); ?>
      <script src="../../lib/jquery-3.2.1.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
      <script src="deleteForn.js"></script>
      <script src="editForn.js"></script>
      <script src="elencoFornitori.js"></script>
      <style media="screen">
        body{
          background-image: url("../../usedImages/burgerLOGO.png");
        }
      </style>
    </head>

    <body>
      <?php require('../../lib/nav.php'); ?>
      <div class="container">
      <div class="row">
    <div class="col s12 l12">
      <ul class="collection">
        </ul>
  </div>
</div>
<div class="col s8 offset-s2 center-align">
  <a class="waves-effect deep-orange darken-4 btn" href="sign_fornitore.php">Aggiungi fornitore</a>
  <a class="waves-effect deep-orange darken-4 btn" href="admin.php">Torna alla pagina iniziale</a>

</div>
<div class="numb">
<ul class="pagination center-align">
</ul>
</div>
</div>

<?php require('../../lib/footer.php');?>
<script type="text/javascript">
$("#accedi").hide();
$("#noti").hide();
$("#shopcart").hide();
$("#registraRist").hide();
$("#accediRist").hide();
$("#shopcart2").hide();
</script>
</body>
</html>
