function usrDelBut(piForn){
  if(confirm("Sei sicuro di volerlo eliminare ?")){
    obj = {"piForn" : piForn};
    dbParam = JSON.stringify(obj);
    console.log(dbParam);
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
      if(this.readyState == 4 && this.status == 200) {
        riaggiornareLista();
      }
    };
    xmlhttp.open("GET", "deleteFornApi.php?id="+dbParam, true);
    xmlhttp.send();

  }
}






























function riaggiornareLista (){
  var page = 0;
  var num_pages = 3;
  $.getJSON("elencoFornitoriApi.php?request=users&page="+page, function(data) {
  var html_code = "";
  console.log(data.length);
  for(var i = 0; i < data.length; i++){
    html_code += '<li class="collection-item avatar"> <i class="material-icons circle">person</i>';
    html_code += '<span class="title">Nome: '+data[i]["nome"]+'</span>'+'<p>Partita Iva: '+data[i]["pi"]+'</p>'+'<p>Indirizzo: '+data[i]["indirizzo"]+'</p>'+'<p>Email: '+data[i]["email"]+'</p>'+'<p>Telefono: '+data[i]["tel"]+'</p>';
    html_code += '<a href="#!" class="secondary-content"><i onclick="usrEditBut('+data[i]["pi"]+')" class="material-icons">edit</i><i  onclick="usrDelBut('+data[i]["pi"]+')" class="material-icons">delete</i></a><div id="'+data[i]["pi"]+'"></div></li>';
  }
  $(".collection").html(html_code);
  });

  $.getJSON("elencoFornitoriApi.php?request=num_pages_users", function(data) {
  		var num_pages = data["num_pages_users"];

      html_code = '<li class="waves-effect"><a><i class="material-icons">chevron_left</i></a></li>';
      for(let i = 0; i < num_pages; i++){
  			html_code += '<li class="waves-effect"><a>'+(i+1)+'</a></li>';
  		}
      html_code += '<li class="waves-effect"><a><i class="material-icons">chevron_right</i></a></li>';

  		$(".numb ul").html(html_code);

  		manage_pages(page, num_pages);

  		$(".pagination li").click(function(){
  			//controllo se ha classe active o disabled
  			if(!$(this).hasClass("disabled") && !$(this).hasClass("active")){
  				var contenuto = $(this).find("a").text();
  				switch(contenuto) {
            case "chevron_right":
              page+=1;
              break;
            case "chevron_left":
              page-=1;
              break;
            default:
              page = contenuto -1;
  				}

          $.getJSON("elencoFornitoriApi.php?request=users&page="+page, function(data) {
          var html_code = "";
          for(var i = 0; i < data.length; i++){
            html_code += '<li class="collection-item avatar"> <i class="material-icons circle">person</i>';
            html_code += '<span class="title">Nome: '+data[i]["nome"]+'</span>'+'<p>Partita Iva: '+data[i]["pi"]+'</p>'+'<p>Indirizzo: '+data[i]["indirizzo"]+'</p>'+'<p>Email: '+data[i]["email"]+'</p>'+'<p>Telefono: '+data[i]["tel"]+'</p>';
            html_code += '<a href="#!" class="secondary-content"><i onclick="usrEditBut('+data[i]["pi"]+')" class="material-icons">edit</i><i  onclick="usrDelBut('+data[i]["pi"]+')" class="material-icons">delete</i></a><div id="'+data[i]["pi"]+'"></div></li>';
          }
          $(".collection").html(html_code);
        });
  				manage_pages(page, num_pages);
  			}
  		});

  	});
}
/*fine riaggiornareLista*/
