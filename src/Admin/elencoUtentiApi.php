<?php
header('Content-Type: application/json');
if(isset($_GET["request"]))
{
    require('../../lib/db_connect.php');

	$rows_per_page = 5;
  switch ($_GET["request"]) {
		case "num_pages_users":
			$stmt = $conn->prepare("SELECT COUNT(*) AS num_utenti FROM utenti");
			$stmt->execute();

			$stmt->bind_result($num_users);
			$stmt->fetch();

			$num_pages_users = ceil($num_users / $rows_per_page);
			$output = array("num_pages_users" => $num_pages_users);
			print json_encode($output);
			break;

		case "users":
			$page = 0;
			if(isset($_GET["page"])){
				$page = $_GET["page"];
			}

			$start_row = $page * $rows_per_page;

			$stmt = $conn->prepare("SELECT * FROM utenti LIMIT ?, ?");
			$stmt->bind_param("ii", $start_row, $rows_per_page);
			$stmt->execute();

			$result = $stmt->get_result();

			$output = array();
			while($row = $result->fetch_assoc()) {
				$output[] = $row;
			}
			$stmt->close();
			print json_encode($output);
			break;
      case "user":
      if(isset($_GET["ids"])){
        $id = $_GET["ids"];
      }
      $query = "SELECT * FROM utenti WHERE utenti.id = $id";
      $res = $conn->query($query);
      while($row = $res->fetch_assoc()) {
        $output[] = $row;
      }
      print json_encode($output);
      break;
	}

}
