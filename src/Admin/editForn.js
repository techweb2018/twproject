function usrEditBut(piForn){
  console.log(piForn);
  $.getJSON("elencoFornitoriApi.php?request=user&ids="+piForn, function(data) {
    html_code = '<div class="row"><div class="col s12 m6"><div id="crd" class="card white"><div class="card-content orange-text"><span class="card-title">Modifica</span><p><input id="nome" name="nome" value="'+data[0]["nome"]+'" type="text" class="validate"><label for="text">Nome</label><input id="tel" value="'+data[0]["tel"]+'" name="tel" type="text" class="validate"><label for="tel">Telefono</label><input id="pi"value="'+data[0]["pi"]+'" name="pIVA" type="text" class="validate" disabled><label for="P.IVA">P.IVA</label><input id="ind" name="ind" type="text" value="'+data[0]["indirizzo"]+'" class="validate"><label for="ind">Indirizzo</label><input id="email" name="email" value="'+data[0]["email"]+'" type="text" class="validate"><label for="text">Email</label></p></div><div class="card-action"><a onclick="salva()" href="#">Salva</a><a onclick="annulla()" href="#">Annulla</a></div></div></div></div>';
    var id = data[0]["pi"];
    $("#"+id).html(html_code);
});
}

function annulla(){
  $("#crd").hide();
}

function salva(){
  var $items = $('#nome,#email,#tel,#ind,#pi');
  var obj = {}
  $items.each(function() {
  obj[this.id] = $(this).val();
})

  var dbParam = JSON.stringify(obj);

  console.log(dbParam);
  var xmlhttp = new XMLHttpRequest();
  xmlhttp.onreadystatechange = function() {
    if(this.readyState == 4 && this.status == 200) {
      riaggiornareLista();

    }
  };

  xmlhttp.open("GET", "editFornApi.php?fornIds="+dbParam, true);
  xmlhttp.send();

  $("#crd").hide();
}


function riaggiornareLista (){
  var page = 0;
  var num_pages = 3;
  $.getJSON("elencoFornitoriApi.php?request=users&page="+page, function(data) {
  var html_code = "";
  console.log(data.length);
  for(var i = 0; i < data.length; i++){
    html_code += '<li class="collection-item avatar"> <i class="material-icons circle">person</i>';
    html_code += '<span class="title">Nome: '+data[i]["nome"]+'</span>'+'<p>Partita Iva: '+data[i]["pi"]+'</p>'+'<p>Indirizzo: '+data[i]["indirizzo"]+'</p>'+'<p>Email: '+data[i]["email"]+'</p>'+'<p>Telefono: '+data[i]["tel"]+'</p>';
    html_code += '<a href="#!" class="secondary-content"><i onclick="usrEditBut('+data[i]["pi"]+')" class="material-icons">edit</i><i  onclick="usrDelBut('+data[i]["pi"]+')" class="material-icons">delete</i></a><div id="'+data[i]["pi"]+'"></div></li>';
  }
  $(".collection").html(html_code);
  });

  $.getJSON("elencoFornitoriApi.php?request=num_pages_users", function(data) {
  		var num_pages = data["num_pages_users"];

      html_code = '<li class="waves-effect"><a><i class="material-icons">chevron_left</i></a></li>';
      for(let i = 0; i < num_pages; i++){
  			html_code += '<li class="waves-effect"><a>'+(i+1)+'</a></li>';
  		}
      html_code += '<li class="waves-effect"><a><i class="material-icons">chevron_right</i></a></li>';

  		$(".numb ul").html(html_code);

  		manage_pages(page, num_pages);

  		$(".pagination li").click(function(){
  			//controllo se ha classe active o disabled
  			if(!$(this).hasClass("disabled") && !$(this).hasClass("active")){
  				var contenuto = $(this).find("a").text();
  				switch(contenuto) {
            case "chevron_right":
              page+=1;
              break;
            case "chevron_left":
              page-=1;
              break;
            default:
              page = contenuto -1;
  				}

          $.getJSON("elencoFornitoriApi.php?request=users&page="+page, function(data) {
          var html_code = "";
          for(var i = 0; i < data.length; i++){
            html_code += '<li class="collection-item avatar"> <i class="material-icons circle">person</i>';
            html_code += '<span class="title">Nome: '+data[i]["nome"]+'</span>'+'<p>Partita Iva: '+data[i]["pi"]+'</p>'+'<p>Indirizzo: '+data[i]["indirizzo"]+'</p>'+'<p>Email: '+data[i]["email"]+'</p>'+'<p>Telefono: '+data[i]["tel"]+'</p>';
            html_code += '<a href="#!" class="secondary-content"><i onclick="usrEditBut('+data[i]["pi"]+')" class="material-icons">edit</i><i  onclick="usrDelBut('+data[i]["pi"]+')" class="material-icons">delete</i></a><div id="'+data[i]["pi"]+'"></div></li>';
          }
          $(".collection").html(html_code);
        });
  				manage_pages(page, num_pages);
  			}
  		});

  	});
}
/*fine riaggiornareLista*/
