  <?php
  session_start();
  $array = $_SESSION["arrayPiatti"];
  $obj = json_decode(json_encode($array), true);

  $totale = 0.0;

for($i = 0; $i < sizeof($array)-1;$i++){
  $totale += ($obj[$i]["prezzo"] * $obj[$i]["quantita"]);
}

?>
  <style>
    #u{
      color: black!important;
    }
  </style>

          <div class="container">
          <div class="row">
        <div class="col s12 l12 center-align">
          <ul id="riepilog" class="collection with-header">
        <li class="collection-header"><h4>Riepilogo ordine</h4></li>
        <?php for($i = 0; $i < sizeof($array)-1; $i++){ ?>
          <li class="collection-item"><div><span id="u" class="badge"><?php echo $obj[$i]["quantita"] ?></span><?php echo $obj[$i]["piatto"] ?><a class="secondary-content"><?php echo $obj[$i]["prezzo"]?>€</a></div></li>
        <?php } ?>

      </ul>
      <p>Totale: <?php echo $totale?>€ </p>
      <a href="../OfferteRistoranti/menu.php" id="mod" class="btn waves-effect deep-orange darken-4">Modifica ordine</a>

    </div>
    </div>
    </div>
