<script>
    $(".readonly").on("keydown paste", function(e){
        e.preventDefault();
    });

</script>
      <link rel="stylesheet" type="text/css" title="stylesheet" href="dataora.css">
      <script src="dataora.js"></script>
 <div class="container">
      <form id="dataora" method="post" action="#">
        <div class="row">
          <h5 class="center-align">Indica data e ora per la consegna</h5>
        <div class="input-field col s12">
          <input id="date" name="date" type="text" class="datepicker" required>
            <label for="date">Data</label>
          </div>
         </div>
         <div class="row">
          <div class="input-field col s12">
            <input id="hour" name="hour" type="text" class="timepicker" required>
              <label for="hour">Ora</label>
            </div>
         </div>
         <div class="row">
       <div class="col s12">
         <h5 class="center-align">Seleziona uno dei due ingressi del campus</h5>
         <div id="map"></div>
         <input id="selection" name="sel" class="readonly" required></input>
       </div>
     </div>
     </div>
  </form>


<script async defer
   src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD1ye4vcHZwcOxe5OBjdg5rqKq3QwFFu7Q&callback=initMap">
</script>
