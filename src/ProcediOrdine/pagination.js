function manage_pages(current_page, num_pages){

  $(".pagination li").removeClass("disabled");
  $(".pagination li").removeClass("active deep-orange darken-2");

	if(current_page == 0){
		$(".pagination li:first-child").addClass("disabled");
    $("#prev").addClass("disabled");
    $("#next").removeClass("disabled");
	}

	if(current_page == num_pages - 1){
		$(".pagination li:last-child").addClass("disabled");
    $("#next").addClass("disabled");
    $("#prev").removeClass("disabled");
	}

	$(".pagination li:nth-child("+(current_page + 2)+")").addClass("active deep-orange darken-2");
  if(current_page == num_pages - 2){
    $("#next").removeClass("disabled");
    $("#prev").removeClass("disabled");
  }
}

$(document).ready(function(){
  $("#shopcart").hide();
  var page = 0;
	var num_pages = 3;
  var links = ["riepilogo.php", "dataora.php", "pagamento.php"];
     $.ajax({
     type: "GET",
     url: links[page],
     dataType: "html"
    }).done(function(data) {
      $('.code').html(data);
    });

  manage_pages(page,num_pages);

$(".pagination li").click(function(){
	if(!$(this).hasClass("disabled") && !$(this).hasClass("active")){
var contenuto = $(this).find("a").text();
switch(contenuto) {
  case "chevron_right":
    page+=1;
    break;
  case "chevron_left":
    page-=1;
    break;
  default:
    page = contenuto -1;
}
$.ajax({
type: "GET",
url: links[page],
dataType: "html"
}).done(function( data ) {
$('.code').html(data);
});
manage_pages(page, num_pages);
}
});


$("#next").click(function(){
      if((page == 1) && ($("#date").val() == "" || $("#hour").val() == "" || $("#selection").val() == "")){
        alert("Compila tutti i campi!");
            return false;
      } else if((page == 1) && !($("#date").val() == "" || $("#hour").val() == "" || $("#selection").val() == "")) {

        var $items = $('#date,#hour,#selection');
        var obj = {}
        $items.each(function() {
          obj[this.id] = $(this).val();
        });

        dbParam = JSON.stringify(obj);
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
          if(this.readyState == 4 && this.status == 200) {
                  console.log("ok");
          }
        };
        console.log(dbParam);
        //window.location.href='mandaOrdine.php?vals='+dbParam;
        xmlhttp.open("GET", "mandaOrdine.php?vals="+dbParam, true);
        xmlhttp.send();
}

  if(!$(this).hasClass("disabled") && !$(this).hasClass("active")){
  var contenuto = $(this).find("a").text();

  page+=1;

  $.ajax({
    type: "GET",
    url: links[page],
    dataType: "html"
  }).done(function( data ) {
    $('.code').html(data);
  });
  manage_pages(page, num_pages);
  }

});

    $("#prev").click(function(){
      if(!$(this).hasClass("disabled") && !$(this).hasClass("active")){
      var contenuto = $(this).find("a").text();

      page-=1;

      $.ajax({
        type: "GET",
        url: links[page],
        dataType: "html"
      }).done(function( data ) {
        $('.code').html(data);
      });

      manage_pages(page, num_pages);
      }
        });
});
