$(document).ready(function(){
    $('.datepicker').datepicker({
      autoClose: true,
      disableWeekends: true,
      minDate: new Date(),
      format: 'yyyy-m-d'
    });
  });

  $(document).ready(function(){
      $('.timepicker').timepicker({
          autoClose: true
      });
  });


  var marker;
  function initMap() {
    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 17,
      center: {lat:  44.147857, lng: 12.235443}
    });

    marker = new google.maps.Marker({
      map: map,
      draggable: true,
      animation: google.maps.Animation.DROP,
      position: {lat: 44.147841, lng: 12.235442},
      icon: {
		      url: "../../usedImages/marker.png",
		      scaledSize: new google.maps.Size(40, 60)
	       }
    });

    marker2 = new google.maps.Marker({
      map: map,
      draggable: true,
      animation: google.maps.Animation.DROP,
      position: {lat: 44.147611, lng: 12.236189},
      icon: {
		      url: "../../usedImages/marker.png",
		      scaledSize: new google.maps.Size(40, 60)
	       }
    });

    marker.setAnimation(google.maps.Animation.BOUNCE);
    marker2.setAnimation(google.maps.Animation.BOUNCE);
    marker.addListener('click', dialogWindow);
    marker2.addListener('click', dialogWindow2);

  }

  function dialogWindow2() {
    var ts;
      if (confirm("Hai selezionato l'entrata di Via Cesare Pavese ")) {
        ts = "Via Cesare Pavese";
      } else {
        ts = "Seleziona un ingresso per la consegna!";
      }

      document.getElementById("selection").value= ts;
  }
  function dialogWindow() {
    var ts;
      if (confirm("Hai selezionato l'entrata di Via Machiavelli")) {
        ts = "Via Machiavelli";
      } else {
        ts = "Seleziona un ingresso per la consegna!";
      }

      document.getElementById("selection").value = ts;
  }
