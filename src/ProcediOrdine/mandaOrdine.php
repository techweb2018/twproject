<?php
header('Content-Type: application/json');
session_start();
require('../../lib/db_connect.php');

$array = $_SESSION["arrayPiatti"];

$usID = $_SESSION["id"];
$piatti = json_decode(json_encode($array), true);
$dataora = json_decode($_GET["vals"], true);

$totale = 0.0;
for($i = 0; $i < sizeof($array)-1;$i++){
$totale += ($piatti[$i]["prezzo"] * $piatti[$i]["quantita"]);
}

$stmt = $conn->prepare("INSERT INTO consegne (pi,userID,data,orario,totale,luogo,consegnato) VALUES (?, ?, ?, ?, ?, ?, ?)");
$stmt->bind_param("sissdsi", $pi, $userID, $data, $orario, $totale, $luogo, $consegnato);
$pi = $piatti[sizeof($array)-1]["partitaIVA"];
$userID = $usID;
$data = $dataora["date"];
$orario =$dataora["hour"];
$totale = $totale;
$luogo = $dataora["selection"];
$consegnato = "0";
$stmt->execute();
$stmt->close();

$stmt = $conn->prepare("SELECT MAX(consegnaID) FROM consegne");
$stmt->execute();
$stmt->bind_result($consID);
$stmt->fetch();

$stmt->close();


for($i = 0; $i < sizeof($array)-1; $i++){
$stmt = $conn->prepare("INSERT INTO cosa (ciboID, consegnaID, quantita) VALUES (?, ?, ?)");
$stmt->bind_param("iii", $ciboID, $consegnaID, $quantita);
$ciboID = $piatti[$i]["cod"];
$consegnaID = $consID;
$quantita = $piatti[$i]["quantita"];;
$stmt->execute();
}
$stmt->close();

$stmt = $conn->prepare("INSERT INTO notificheFornitore (pi, valore, consegnaID, data, ora) VALUES (?, ?, ?, ?, ?)");
$stmt->bind_param("isiss", $pi, $valore, $consegnaID, $data, $ora);
$pi = $piatti[sizeof($array)-1]["partitaIVA"];
$valore= "Il cliente ".$usID." ha appena effettuato l'ordine ".$consID." presso il ristorante";
$consegnaID=$consID;
$data= date(DATE_ATOM);
$ora= date('H:i:s');
$stmt->execute();
$stmt->close();

?>
