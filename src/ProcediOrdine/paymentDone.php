<!DOCTYPE html>
  <html lang="it-IT">
    <head>
      <?php require('../../lib/header.php'); ?>
      <script src="../../lib/jquery-3.2.1.min.js"></script>
      <script src="pagination.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    </head>

    <body>
        <?php require('../../lib/nav.php'); ?>



<div class="container">
  <div class="row">
<div class="col s8 offset-s2 center-align">
 <h5 class="center-align"> </br> </br> </br>Grazie! </br> Il tuo ordine è stato ricevuto, </br> riceverai una notifica non appena il fattorino lascerà il ristorante. </br> </br> </br> </br></h5>
 <a href="../Utonti/clienti.php"class=" waves-effect btn"><i class="material-icons left">person</i>Vai al tuo profilo</a> </br>  </br>
 <a href="../HomeP/HomeP.php"class=" waves-effect btn"><i class="material-icons left">home</i>Torna alla home</a>
</div>
</div>
</div>

 <?php require('../../lib/footer.php');?>
</body>
</html>
