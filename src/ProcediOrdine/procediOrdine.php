<?php
session_start();
require('../../lib/db_connect.php');

$obj = json_decode($_GET["ordine"], false);
$_SESSION["arrayPiatti"] = $obj;

$array = $_SESSION["arrayPiatti"];

?>
<!DOCTYPE html>
  <html lang="it-IT">
    <head>
      <?php require('../../lib/header.php'); ?>
      <script src="../../lib/jquery-3.2.1.min.js"></script>
      <script src="pagination.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>

    </head>
    <body>
        <?php require('../../lib/nav.php'); ?>

        <div class="code"></div>

       <div class="container">
      <div class="row">
      <div class="col s8 offset-s2 center-align">
     <button id="prev" class="btn waves-effect deep-orange darken-4" type="submit" name="action">Indietro</button>
     <button id="next" class="btn waves-effect deep-orange darken-4" type="submit" name="action">Prosegui</button>

        <ul class="pagination">
        <li class="waves-effect"><a><i class="material-icons">chevron_left</i></a></li>
        <li class="waves-effect disabled"><a>1</a></li>
        <li class="waves-effect disabled "><a>2</a></li>
        <li class="waves-effect disabled "><a>3</a></li>
        <li class="waves-effect disabled"><a><i class="material-icons">chevron_right</i></a></li>
        </ul>
      </div>
      </div>
      </div>

      <?php require('../../lib/footer.php');?>
  </body>
  </html>
