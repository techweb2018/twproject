<script>
    function selectedItemBtn(elem){
      var arr = $(".collection-item");

      for(var i = 0; i < arr.length;i++ ){
          $(arr[i]).removeClass("collection-item active");
          $(arr[i]).addClass("collection-item");
      }
      $(elem).addClass("collection-item active");
    }
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>

        <div class="container">
          <div class="row">
      <div class="col s8 offset-s2 center-align">
         <h5 class="center-align">Seleziona il tipo di pagamento</h5>

        <div class="collection center-align">
         <a onclick="selectedItemBtn(this);" href="#!" class="collection-item">Mastercard</a>
         <a onclick="selectedItemBtn(this);" href="#!" class="collection-item">Visa</a>
         <a onclick="selectedItemBtn(this);" href="#!" class="collection-item">American Express</a>
         <a onclick="selectedItemBtn(this);" href=" #!" class="collection-item">PayPal</a>
       </div>
       <a class=" waves-effect btn" href="paymentDone.php"><i class="material-icons left">payment</i>PAGA ORA</a>
