<?php
session_start();
require('../../lib/db_connect.php');
  //CARICA TUTTI I FORNITORI PRESENTI NEL DB
  $sql1 = "SELECT * FROM fornitori";
  $sql2= "SELECT pi FROM fornitori WHERE nome = id";
  $result =$conn->query($sql1);
  if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
  }
?>


<!DOCTYPE html>
<html lang="it-IT">
  <head>
    <meta charset="UTF-8"/>
    <meta name="description"
        content="Lista fornitori disponibili, visualizzata dai possibili clienti"/>
    <meta name="author" content="Giulia Brugnatti"/>
		<title>UniChow - MyProfile</title>
    <?php require('../../lib/header.php'); ?>
    <script src="../../lib/jquery-3.2.1.min.js"></script>
    <link rel="stylesheet" type="text/css" title="stylesheet" href="style.css">
  </head>


  <body>
    <?php require('../../lib/nav.php'); ?>

    <div class="section section blue lighten-2">
      <div class="row container">
        <header>
          <h1 class ="center-align">Ristoranti</h1>
        </header>
      </div>
    </div>

    <div id ="container" class="section white">

     <table class="highlight" id ="table">
       <thead>
         <tr>
              <th data-field="foto">    </th>
              <th data-field="id">    </th>
             <th data-field="nomeRistorante">Nome Ristorante</th>
             <th data-field="consegna"> Modalità di consegna</th>
         </tr>
       </thead>

       <tbody>
         <?php
               if ($result->num_rows > 0) {
                  while($row =$result->fetch_assoc()){
         ?>
         <tr>
           <td> <img src="../../images/<?php echo $row["imgF"]; ?>" width="40" height="40"></td>
           <td> <?php echo "<td class='id'>" . $row["nome"] . "\n" ; ?></td>
           <td> <div> <?php echo " Costo consegna   " .$row["costoConsegna"]; ?>
           <a class="secondary-content"href="#!"><i class="material-icons">send</i></a></td></div>
         </tr>
         <?php
       }
       }
       ?>
       </tbody>
     </table>
     <script type="text/javascript">
    $("tr").click(function() {
        var id = $(this).find('.id').text();
        if (id != "") {//alert("Your data is: " + $.trim(id));
          window.location.href = "menu.php?nome=" + id;
      }
    });
    </script>
    </div>
    <?php require('../../lib/footer.php');?>
  </body>
</html>
