<?php
  session_start();
  require('../../lib/db_connect.php');
  if(isset($_GET["nome"])){             //se il caricamento della pagine viene forzato c'è il rischio di perdere il nome del fornitore perciò
    $_SESSION["nome"]=$_GET["nome"];    //se la get riesce a beccare la variabile fornitore salva tale valore in variabile di sessione
    $nomeFornitore=$_GET["nome"];       //poi la salva anche nella variabile usata per le query
  }else{                                //se per qualche ragione la GET non va a buon fine
    $nomeFornitore=$_SESSION["nome"];   //il nome del ristorante prescelto dall'utente viene caricato dalla variabile di sessione
  }

  $sql1 = "SELECT pi FROM fornitori WHERE nome = '$nomeFornitore'"; //query che dato il nome del fornitore ricava il suo codice univoco
  $result = $conn->query($sql1);
  if ($conn->connect_error) {                                       //Se la connessione da dei problemi stampo un msg di errore e festa finita
    die("Connection failed: " . $conn->connect_error);
  }
  if ($result->num_rows > 0) {                                      //se la query restituisce un qualche valore
     while($row =$result->fetch_assoc()){
       $fornitoreID=$row["pi"];
       $_SESSION["Fornitoreid"]=$fornitoreID;                       //salvo l'id del fornitore in una variabile di sessione per pterlo poi aggiungere nell'array associativo
     }
   }
  $sql2 = "SELECT cibo.ciboID, cibo.nome, cibo.cat, cibo.prezzo, cibo.imgC FROM cibo, fornitori JOIN offerte ON fornitori.pi = offerte.pi WHERE cibo.ciboID = offerte.ciboID AND fornitori.pi = $fornitoreID";
  $result =$conn->query($sql2);
?>

<!DOCTYPE html>
<html lang="it-IT">
  <head>
    <meta charset="UTF-8"/>
    <meta name="description"
        content="Pagina del menu' del fornitore scelto dall'utente"/>
    <meta name="author" content="Giulia Brugnatti"/>
		<title>UniChow - MyProfile</title>
    <?php require('../../lib/header.php'); ?>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>

    <script src="../../lib/jquery-3.2.1.min.js"></script>
    <style media="screen">

    </style>
    <link rel="stylesheet" type="text/css" title="stylesheet" href="style.css">
    </head>
    <body>
      <?php require('../../lib/nav.php'); ?>
      <div class="section blue lighten-2">
        <div class="row container">
          <header>
            <h1 class ="center-align"> <?php echo $nomeFornitore ?></h1>
          </header>
        </div>
      </div>

    <div id ="container" class="section white">
      <h2>Cibo</h2>

         <table class="highlight">
           <thead>
             <tr>
                <th hidden id="c"> Cod</th>
                <th > </th>
                 <th id="np"> Nome Piatto</th>
                 <th id="c"> Categoria</th>
                 <th > Prezzo</th>
             </tr>
           </thead>

           <tbody>


             <?php
                   if ($result->num_rows > 0) {
                      while($row =$result->fetch_assoc()){
                        if($row["cat"]!="Bevanda" ){
             ?>
             <tr>
               <td class="id" hidden> <?php echo $row["ciboID"]; ?></td>
               <td> <img src="../../images/<?php echo $row["imgC"]; ?>" width="100" height="100"/></td>
               <td class="no"> <?php echo $row["nome"]; ?></td>
               <td> <?php echo $row["cat"]; ?></td>
               <td class="pr"> <?php echo $row["prezzo"]; ?></td>
               <td class="secondary-content">
                 <p class = "remove">
                   <i> <a onclick="M.toast({html: 'Piatto rimosso'})" class="material-icons blue-text" href="#!">remove_circle</a></i>
                </p>
                <p class = "add">
                  <i> <a onclick="M.toast({html: 'Piatto aggiunto'})" class="material-icons blue-text" href="#!">add_box </a> </i>
                </p>
               </td>
             </tr>
             <?php
                      }
                    }
                  }
           ?>
           </tbody>

         </table>
           <ul id="nav-mobile" class="right hide-on-med-and-down">
           </ul>
     </div>

     <div id ="container" class="section white">
       <h2>Bevande</h2>
        <table class="highlight">
           <thead>
             <tr>
                <th hidden id="c"> Cod</th>
                <th > </th>
                 <th id="nb"> Nome Bevanda</th>
                 <th id="c"> Categoria</th>
                 <th id="pr"> Prezzo</th>
             </tr>
           </thead>
           <tbody>
             <?php
             $result =$conn->query($sql2);
                   if ($result->num_rows > 0) {
                      while($row =$result->fetch_assoc()){
                        if($row["cat"]=="Bevanda" ){
             ?>
             <tr>
               <td class="id" hidden> <?php echo $row["ciboID"] ?></td>
               <td> <img src="../../images/<?php echo $row["imgC"]; ?>" width="90" height="90"/></td>
               <td class="no"> <?php echo $row["nome"];?></td>
               <td> <?php echo $row["cat"]; ?></td>
               <td class="pr"> <?php echo $row["prezzo"] ?></td>
               <td class="secondary-content">
                 <p class = "remove">
                   <i> <a  onclick="M.toast({html: 'Bevanda rimossa'}) "class="material-icons blue-text"href="#!">remove_circle </a> </i>
                </p>
                <p class = "add">
                  <i> <a onclick="M.toast({html: 'Bevanda aggiunta'})" class="material-icons blue-text " href="#!">add_box </a> </i>
                </p>
               </td>
             </tr>
             <?php
                      }
                    }
                  }
           ?>
           </tbody>
         </table>
         <ul id="nav-mobile" class="right hide-on-med-and-down"></ul>
   </div>

    <div class="row container">
      <div class="row">
        <div class="center-align">
          <p>
                  <button class="btn waves-effect waves-light blue lighten-2 pulse" type="submit" name="action" id="button">Procedi ordine
                   <i class="material-icons right">send</i>

                </button>

          </p>
        </div>
      </div>
    </div>
    <?php require('../../lib/footer.php');?>
  </body>
  <script>
    var flag=0;           //variabile che indica se è stato premuto il botton - o quello +
    let arrayOrdine = []; //array che contiene l'ordine del cliente
    var elementsInArray=0;  //variabile usata per capire la quantità totale di elementi sono presenti in totale nell'array

    //variabili che contengono i dati del piatto sul quale ha cliccato l'utente
    var piatto;
    var prezzo;
    var id;
    var quantita=0;

    function send(){
      arrayOrdine.push({partitaIVA: <?php echo $_SESSION["Fornitoreid"]; ?>});
      myjson= JSON.stringify(arrayOrdine);
      window.location.href = "../ProcediOrdine/procediOrdine.php?ordine="+myjson;
    }

    function addEntry(p, q, pr, id){ //funzione che aggiunge materialmente il piatto e la relativa quantità all'array
      arrayOrdine.push({piatto: p, quantita: q, prezzo: pr, cod: id});
      count();
      update();
    }

    function count(){ //Funzione che conta quanti elementi sono effettivamente presenti nell'array
      elementsInArray=0;
      for (let entry of arrayOrdine) {//ciclo che controlla se il piatto è già presente nella Lista
        elementsInArray=elementsInArray+entry.quantita;
      }
    }

    function remove(){//funzione che aggiorna il valore di quantità e in caso richiama la funzione che si occuperà di aggiungere materialmente l'emeneto all'array
      quantita=quantita-1;                     //alert($.trim(piatto) + " " + "Rimosso" +" " +  $.trim(quantita));
      if(quantita>0){     //se la quantità del piatto è ancora maggiore di 0 lo riaggiungo, se no evito
        addEntry(piatto, quantita, prezzo, id);
      }
    }

    function add(){//funzione che aggiorna il valore di quantità e richiama la funzione che si occuperà di aggiungere materialmente l'emeneto all'array
      quantita=quantita+1;
      addEntry(piatto, quantita, prezzo, id);//alert($.trim(piatto) + " " + "Aggiunto" + " " + $.trim(quantita));
    }

    function update(){
      var html_code = '<a id="shopcart" href="#!"><i class="material-icons left">shopping_cartt</i>Carrello '+ elementsInArray +'<span class="badge"></span></a>';
      $("#carr").html(html_code);
    }

    $( ".remove" ).click(function() { //se viene cliccato - imposto il valore di flag a 1 in modo da sapere che l'elemento è da rimuovere dall'array
      flag=1;
     });

    $( ".add" ).click(function() { //se viene cliccato + imposto il valore di flag a 2 in modo da sapere che l'elemento è da aggiungere all'array
     flag=2;
    });

    $( "#button" ).click(function() { //se viene cliccato Procedi all'ordine, la funzione passa l'array alla pagina di Lindina
      send();
    });

    $("#carr").click(function() { //se viene cliccato il carrello, la funzione passa l'array alla pagina di Lindina
      send();
    });

    $("tr").click(function() {  //funzione che identifica su quale elemento è statio cliccato + o -
      //salvo tutte le informazioni relative al piatto che dovrò poi inserire nell'array
      piatto = $(this).find('.no').text();
      prezzo = $(this).find('.pr').text();
      id = $(this).find('.id').text();
      quantita=0;
     for (let entry of arrayOrdine) {  //ciclo che controlla se il piatto è già presente nella Lista
       if (entry.piatto==piatto) {
           quantita=entry.quantita;    //se è presente si salva la quantità relativa
           arrayOrdine.splice(arrayOrdine.indexOf(entry), 1);  //elimina tutto l'elemento (piatto + relativa quantità)
        }
       }
       if(flag==1){                      //flag = 1 è stato cliccato rimuovi
         remove();
       }else{                            // se è stato cliccato aggiungi
         add();
       }
       count();
       update();
       console.log(arrayOrdine);
    });

    $(document).ready(function(){
     $("#noti").hide();
     $("#shopcart").hide();
     update();
    });
  </script>
</html>
