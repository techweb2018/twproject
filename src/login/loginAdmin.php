<?php
  session_start();
  if (isset($_POST["usr"]) && isset($_POST["pwd"])) {
      require('../../lib/db_connect.php');

      if($stmt = $conn->prepare('SELECT username, password FROM admin WHERE username = ?')) {
        $stmt->bind_param('s', $_POST["usr"]);
        if($stmt->execute() !== FALSE){

          $stmt->bind_result($usr, $pswrd);
          $stmt->fetch();

          if($_POST["pwd"] == $pswrd) {
            $_SESSION["logged_in"] = TRUE;
            $_SESSION["name"] = $_POST["usr"];

            header("Location: ../Admin/admin.php");
          } else {
            //errore di inserimento
            $_SESSION["logged_in"]=FALSE;
          }
        }
      }
      $stmt->close();
  }
?>

<!DOCTYPE html>
<html lang="it-IT">
  <head>
    <meta charset="UTF-8"/>
    <meta name="description"
        content="Login"/>
    <meta name="author" content="Filippo Paganelli"/>
		<title>UniChow - MyProfile</title>
    <?php require('../../lib/header.php'); ?>
    <script src="../../lib/jquery-3.2.1.min.js"></script>
  </head>
  <body>
    <div class="section  deep-orange darken-2">
      <div class="row container">
        <header>
          <h1 class ="center-align">Unichow</h1>
        </header>
        </div>
    </div>
    <div id ="container" class="section white">
        <div class="row container">
          <div class="row">
              <form class="col s12" method="post" action="#">
                <div class="row">
                  <div class="input-field col s6 offset-s3">
                    <input id="username" name="usr" type="text" class="validate" required pattern=".{4,}">
                    <label for="username">Username</label>
                    <span class="helper-text" data-error="wrong" data-success="right"> </span>
                  </div>
                </div>
                <div class="row">
                  <div class="input-field col s6 offset-s3">
                    <input id="password" name="pwd" type="password" class="validate" required pattern=".{8,}">
                    <label for="password">Password</label>
                  </div>
                </div>
                <div class="row center-align">
                  <button class="btn waves-effect waves-light" type="submit" name="action" >Accedi
                    <i class="material-icons right">send</i>
                  </button>
                </div>
              </form>
            </div>
        </div>
    </div>
    <?php require('../../lib/footer.php');?>
  </body>
</html>
