<?php
  session_unset();
  session_start();
  require('../../lib/db_connect.php');

  // se è gia stato effettuato il login dal fornitore accedo direttamente alla pagina senza effettuare il login
  if(isset($_SESSION["logged_inF"])){
    if($_SESSION["logged_inF"] == TRUE){
      header("Location: ../Fornitore/fornitore.php");
    } else {
      //altrimenti si effettua il login tramite i dati inseriti nella form
      if (isset($_POST["pi"]) && isset($_POST["pwd"])) {
          if($stmt = $conn->prepare('SELECT pi, nome, password FROM fornitori WHERE pi = ?')) {
            $stmt->bind_param('s', $_POST["pi"]);
            $pw = $_POST["pwd"];

            if($stmt->execute() !== FALSE) {
              $stmt->bind_result($pi, $name, $pswrd);
              $stmt->fetch();

              // se il login ha successo accedo alla pagina del fornitore
              if(password_verify($pw, $pswrd)) {
                $_SESSION["logged_inF"] = TRUE;
                $_SESSION["pi"] = $pi;
                header("Location: ../Fornitore/fornitore.php");
              } else {
                $_SESSION["logged_inF"] = FALSE;
              }
            }
          }
          $stmt->close();
      }
    }
  }

?>

<!DOCTYPE html>
<html lang="it-IT">
  <head>
    <meta charset="UTF-8"/>
    <meta name="description"
        content="Login"/>
    <meta name="author" content="Filippo Paganelli"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>UniChow - MyProfile</title>
    <?php require('../../lib/header.php'); ?>
    <script src="../../lib/jquery-3.2.1.min.js"></script>
  </head>
  <body>
    <div class="section  deep-orange darken-2">
      <div class="row container">
        <header>
          <h1 class ="center-align">Unichow</h1>
        </header>
        </div>
    </div>
    <div id ="container">
        <div class="row container">
          <div class="row">
              <form class="col s12" method="post" action="#">
                <div class="row">
                  <div class="input-field col s6 offset-s3">
                    <input id="pi" name="pi" type="text" class="validate" required pattern=".{11,}">
                    <label for="pi">Partità IVA</label>
                    <span class="helper-text" data-error="wrong" data-success="right"></span>
                  </div>
                </div>
                <div class="row">
                  <div class="input-field col s6 offset-s3">
                    <input id="password" name="pwd" type="password" class="validate" required pattern=".{8,}">
                    <label for="password">Password</label>
                    <span class="helper-text" data-error="wrong" data-success="right"></span>
                  </div>
                </div>
                <div class="row center-align">
                  <button class="btn waves-effect waves-light" type="submit" name="action" >Accedi
                    <i class="material-icons right">send</i>
                  </button>
                </div>
              </form>
            </div>
        </div>
    </div>
    <?php require('../../lib/footer.php');?>
  </body>
</html>
