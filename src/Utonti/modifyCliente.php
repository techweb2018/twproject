<?php
  require('../../lib/db_connect.php');
  session_start();
  if (isset($_POST["nome_att"]) && isset($_POST["email"]) && isset($_POST["cognome"]) && isset($_POST["tel"]) && isset($_POST["username"])) {


   $stmt = $conn->prepare("UPDATE utenti SET nome = ?, email = ?, cognome = ?, tel = ?, username = ? WHERE id = ?");
   $stmt->bind_param("sssisi", $nome, $email, $cognome, $tel, $username, $id);

   // eliminazione caratteri pericolosi e hash password
   $id = $_SESSION["id"];

   $nome = mysqli_real_escape_string($conn, $_POST["nome_att"]);
   $email = mysqli_real_escape_string($conn, $_POST["email"]);
   $cognome = mysqli_real_escape_string($conn, $_POST["cognome"]);
   $tel = mysqli_real_escape_string($conn, $_POST["tel"]);
   $username = mysqli_real_escape_string($conn, $_POST["username"]);

   if($stmt->execute() == TRUE) {
     header("Location: clienti.php");
   } else {
     $_SESSION["logged_in"] = FALSE;
   }
    $stmt->close();
 }

?>

<!DOCTYPE html>
  <html lang="it-IT">
    <head>
      <meta charset="UTF-8"/>
      <meta name="description"
          content="Pagina di registrazione per i fornitori di cibo nella zona di Cesena"/>
      <meta name="author" content="Giulia Brugnatti"/>
      <title>UniChow - MyProfile</title>
      <?php require('../../lib/header.php'); ?>
      <script src="../../lib/jquery-3.2.1.min.js"></script>
    </head>
    <body>
      <div class="section  deep-orange darken-2">
        <div class="row container">
          <header>
            <h1 class ="center-align">Unichow</h1>
          </header>
          </div>
      </div>
      <div id ="container">
        <div class="row container">
          <div class="row">
              <form class="col s12" method="post" action="#">
                <div class="row">
                  <div class="input-field col s3 offset-s3">
                    <input id="first_name" name="nome_att" type="text" value="<?php echo $_SESSION["nome"]; ?>" class="validate" required pattern=".{4,}">
                    <label for="first_name">Nome</label>
                    <span class="helper-text" data-error="wrong" data-success="right"> </span>
                  </div>
                  <div class="input-field col s3">
                    <input id="email" name="email" type="email" value="<?php echo $_SESSION["email"]; ?>" class="validate" required pattern=".{4,}">
                    <label for="email">Email</label>
                    <span class="helper-text" data-error="wrong" data-success="right"> </span>
                  </div>
                </div>
                <div class="row">
                  <div class="input-field col s3 offset-s3">
                    <input id="cognome" name="cognome" type="text" value="<?php echo $_SESSION["cognome"]; ?>" class="validate" required pattern=".{4,}">
                    <label for="cognome">Cognome</label>
                    <span class="helper-text" data-error="wrong" data-success="right"> </span>
                  </div>
                  <div class="input-field col s3">
                    <input id="tel" name="tel" type="tel" class="validate" value="<?php echo $_SESSION["tel"]; ?>" required pattern=".{8,}">
                    <label for="tel">Telefono</label>
                    <span class="helper-text" data-error="wrong" data-success="right"> </span>
                  </div>
                </div>
                <div class="row">
                  <div class="input-field col s3 offset-s3">
                    <input id="username" name="username" type="text" value="<?php echo $_SESSION["username"]; ?>" class="validate" required pattern=".{4,}">
                    <label for="username">Nome utente</label>
                    <span class="helper-text" data-error="wrong" data-success="right"> </span>
                  </div>

                </div>
                <div class="row center-align">
                  <button class="btn waves-effect waves-light" type="submit" name="action">Conferma
                    <i class="material-icons right">send</i>
                  </button>
                </div>
              </form>
            </div>
        </div>
      </div>
      <?php
        require('../../lib/footer.php');
      ?>
    </body>
</html>
