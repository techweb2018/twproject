<?php
  session_start();
  require('../../lib/db_connect.php');

  if($stmt = $conn->prepare("SELECT id, username, email, nome, cognome , tel FROM utenti WHERE id = ?")){
    $stmt->bind_param('s', $_SESSION["id"]);
    $stmt->execute();
    $stmt->store_result();
    $stmt->bind_result($id, $username, $email,  $nome, $cognome, $tel);
    $stmt->fetch();
    $_SESSION["id"]=$id;
    $_SESSION["username"] = $username;
    $_SESSION["email"]= $email;
    $_SESSION["nome"]= $nome;
    $_SESSION["cognome"]=$cognome;
    $_SESSION["tel"]=$tel;
  }
?>

<!DOCTYPE html>
  <html lang="it-IT">
    <head>
      <meta charset="UTF-8"/>
      <meta name="description"
          content="Pagina Personale dell'utente registrato"/>
      <meta name="author" content="Giulia Brugnatti"/>
      <title>UniChow</title>
      <?php require('../../lib/header.php'); ?>
      <script src="../../lib/jquery-3.2.1.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
      <script src="elenchi.js"></script>
      <script src="modifyLayout.js"></script>
    </head>
    <body>
      <?php require('../../lib/nav.php'); ?>
      <div id="container" class="container">
        <div id="notifySpace" class="row">
        </div>
        <div class="row">
          <div class="col s5">
            <ul class="collection with-header">
              <li class="collection-header"><h4>I miei dati:</h4></li>
              <li class="collection-item">Nome: <?php echo $nome; ?></li>
              <li class="collection-item">Cognome: <?php echo $cognome; ?></li>
              <li class="collection-item">Email: <?php echo $email; ?></li>
              <li class="collection-item">Telefono: <?php echo $tel; ?></li>
              <li class="collection-item"><div>Modifica Dati<a href="modifyCliente.php" class="secondary-content"><i class="material-icons">settings</i></a></div></li>
              <li class="collection-item"><div>Modifica Password<a href="modifyPassword.php" class="secondary-content"><i class="material-icons">settings</i></a></div></li>
              <li class="collection-item"><div>Elimina Account<a href="#!" onclick="showAttempt(<?php echo $id; ?>)"class="secondary-content"><i class="material-icons">delete</i></a></div></li>
              <ul id="deleteAcc"></ul>
            </ul>
          </div>
          <div class="col s5">
            <ul class="collection with-header">
              <li class="collection-header"><h4>I miei ordini:</h4></li>
              <ul id="ordini"></ul>
            </ul>

          <a class="btn waves-effect waves-light" type="submit" name="action" href="../OfferteRistoranti/elenco_ristoranti.php" id="button">SCEGLI IL RISTORANTE<i class="material-icons right"> send</i></a>
          </div>
        </div>
      </div>
      <?php require('../../lib/footer.php');?>
      <script type="text/javascript">
      $("#asd").hide();
      $("#registraRist").hide();
      $("#accediRist").hide();
      </script>
    </body>
  </html>
