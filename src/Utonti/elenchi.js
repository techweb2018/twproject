function showOrdini(){
    var result;

    $.getJSON("elencoOrdini.php", function(data){
      var html_code = "";
      console.log(data.length);
      for(var i = 0; i < data.length; i++){
        html_code += '<li class="collection-item"><div>Ordine: '+data[i]["consegnaID"]+' Luogo: '+data[i]["luogo"]+' Data: '+data[i]["data"]+' Ora: '+data[i]["orario"]+' Totale: '+data[i]["totale"];
        html_code += '<a href="consegna.php?id='+data[i]["consegnaID"]+'"></a></div></li>';

      }
      result = html_code;
      $("#ordini").html(html_code);
    });
      return result;
  }

  function deleteAccount(id){
    obj = {"id" : id};
    dbParam = JSON.stringify(obj);
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
      if(this.readyState == 4 && this.status == 200){
        console.log("Account eliminato");
        console.log(id);
      }
    };
    xmlhttp.open("GET", "deleteAccount.php?id="+dbParam, true);
    xmlhttp.send();
  }


  function hideAttempt(){
    $("#deleteAcc").hide();
  }

  function showAttempt(id){
    $("#deleteAcc").show();
    html_code = '<div class="card blue-grey darken-1">'+
                  '<div class="card-content white-text">'+
                  '<span class="card-title">Eliminazione Account</span>'+
                  '<p>Confermare se si vuole procedere con la eliminazione del proprio account.</p>'+
                  '</div><div class="card-action"><a href="../HomeP/HomeP.php" onclick="deleteAccount('+id+')" >Si</a><a href="#!" onclick=" hideAttempt()">No</a></div></div>';
    $("#deleteAcc").html(html_code);
  }

  $(document).ready(function(){
    showOrdini();
  });
