<?php
  require('../../lib/db_connect.php');
  session_start();
  if (isset($_POST["pwVecchia"]) && isset($_POST["pwNuova"]) && isset($_POST["pwNuovaConf"])) {

    $id = $_SESSION["id"];
    $query = "SELECT password FROM utenti WHERE id = $id";
    $res = $conn->query($query);
    if($res !== FALSE){
      $row = $res->fetch_assoc();
      $pw = $row["password"];
    }

    if(password_verify($_POST["pwVecchia"], $pw)){
      if(strcmp($_POST["pwNuova"], $_POST["pwNuovaConf"]) === 0){
        $stmt = $conn->prepare("UPDATE utenti SET password = ? WHERE id = ?");
        $stmt->bind_param("si", $pwhashed, $_SESSION["id"]);
        $pwhashed = password_hash($_POST["pwNuova"], PASSWORD_DEFAULT);
        if($stmt->execute() === TRUE) {
          header("Location: clienti.php");
        } else {
          $_SESSION["logged_in"] = FALSE;
        }
      }
    }
  }
?>

<!DOCTYPE html>
  <html lang="it-IT">
    <head>
      <meta charset="UTF-8"/>
      <meta name="description"
          content="Pagina di registrazione per i fornitori di cibo nella zona di Cesena"/>
      <meta name="author" content="Filippo Paganelli"/>
      <title>UniChow - MyProfile</title>
      <?php require('../../lib/header.php'); ?>
      <script src="../../lib/jquery-3.2.1.min.js"></script>
    </head>
    <body>
      <div class="section  deep-orange darken-2">
        <div class="row container">
          <header>
            <h1 class ="center-align">Unichow</h1>
          </header>
          </div>
      </div>
      <div id ="container">
        <div class="row container">
          <div class="row">
              <form class="col s12" method="post" action="#">
                <div class="row">
                  <div class="input-field col s6 offset-s3">
                    <input id="password" name="pwVecchia" type="password" class="validate" required pattern=".{8,}">
                    <label for="password">Inserire la vecchia Password</label>
                    <span class="helper-text" data-error="wrong" data-success="right"> </span>
                  </div>
                </div>
                <div class="row">
                  <div class="input-field col s6 offset-s3">
                    <input id="password" name="pwNuova" type="password" class="validate" required pattern=".{8,}">
                    <label for="password">Inserire la nuova Password</label>
                    <span class="helper-text" data-error="wrong" data-success="right"> </span>
                  </div>
                </div>
                <div class="row">
                  <div class="input-field col s6 offset-s3">
                    <input id="password" name="pwNuovaConf" type="password" class="validate" required pattern=".{8,}">
                    <label for="password">Reinserire la nuova Password</label>
                    <span class="helper-text" data-error="wrong" data-success="right"> </span>
                  </div>
                </div>
                <div class="row center-align">
                  <button class="btn waves-effect waves-light" type="submit" name="action">Conferma
                    <i class="material-icons right">send</i>
                  </button>
                </div>
              </form>
            </div>
        </div>
      </div>
      <?php
        require('../../lib/footer.php');
      ?>
    </body>
</html>
